
var clients = new Array();
clients.length = 0;
var broadcast = function(clients, message) {
  var length = clients.length,
    element = null;
  for (var i = 0; i < length; i++) {
    port = clients[i];
    port.postMessage(message);
  }
}

self.addEventListener("connect", function (e) {
    var port = e.ports[0];

    clients.push(port);

    port.addEventListener("message", function (e) {
/////////////////////////////////////////////////////////////////
var data = e.data;
var refresh = e.data.refresh;
//setInterval(() => {
var x= e.data.x;
var y= e.data.y;
var z= e.data.z;
var sclx= e.data.sclx;
var scly= e.data.scly;
var sclz= e.data.sclz;
var rotation= e.data.rotation;
var rotationRx= e.data.rotationRx;
var rotationRy= e.data.rotationRy;
var rotationZ= e.data.rotationZ;
var rotationY = e.data.rotationY;
var rotationX = e.data.rotationX;
var speed = e.data.speed;
var offsetx = e.data.offsetx;
var offsety = e.data.offsety;
var Ysub = e.data.Ysub;
var Yadd = e.data.Yadd;
var Xsub = e.data.Xsub;
var Xadd = e.data.Xadd;
var XYco = e.data.XYco;
var YXco = e.data.YXco;
var XYfo = e.data.XYfo;
var YXfo = e.data.YXfo;
var XYfp = e.data.XYfp;
var YXfp = e.data.YXfp;
var isW = e.data.isW;
var isS = e.data.isS;
var isD = e.data.isD;
var isA = e.data.isA;
var YcursoraxisX = e.data.YcursoraxisX;
var XcursoraxisY = e.data.XcursoraxisY;
var floor1Y = e.data.floor1Y;
var floor1X = e.data.floor1X;
///////////////////////////////////// Mouse
/*
var pageX = e.data.m1;
var pageY = e.data.m2;
var MouseRxresult = -pageX - pageY/2,
var MouseRyresult = pageY*2,
var MouseRxyresult = pageY/10,
var MouseRXyresult = -pageX,
var MouseFresultX = pageX,
var MouseFresultY = pageY;  
*/
///////////////////////////////////// Movement BI
if (isW){
  y=Ysub;
   // x += speed * Math.sin(-rotation * Math.PI / 180);//Xadd
   // y -= speed * Math.cos(-rotation * Math.PI / 180);//Ysub
}
if (isS){
  y=Yadd;
    //x -= speed * Math.sin(-rotation * Math.PI / 180);
    //y += speed * Math.cos(-rotation * Math.PI / 180);
}

if (isD){
  x=Xadd;
    //y += speed * Math.cos(rotation/.25 * Math.PI / 180);
    //x += speed * Math.sin(rotation/.25 * Math.PI / 180);
    
}
if (isA){
  x=Xsub;
   // x -= speed * Math.sin(rotation/.25 * Math.PI / 180);
   // y -= speed * Math.cos(rotation/.25 * Math.PI / 180);
}
Ysub = y - speed,
Yadd = y + speed,
Xsub = x - speed,
Xadd = x + speed,
XYco = x - offsetx,
YXco = y - offsety,
XYfo = -rotationY - offsetx,
YXfo = -rotationX - offsety,
XYfp = rotationX + offsetx,
YXfp = rotationY + offsety;
///////////////////////////////////// Glue code
YcursoraxisX=XYco,
XcursoraxisY=YXco,
floor1Y=XYfo,
floor1X=YXfo,
//rotationX=Xadd;
//rotationY=Yadd,
YplayeraxisX= XYfp,
XplayeraxisY= YXfp;
///////////////////////////////////// Movement BI
var protoR = e.data.protoR;  
var protoR = [];
var ronMatrix = [];
var PIh=Math.PI / 180;
var RangX=rotationRx,RangY=rotation,RangZ=0,
  Rcosz=Math.cos(RangZ * PIh),Rsinzp=Math.sin(RangZ * PIh),Rsinzn=Math.sin(-RangZ * PIh),
  Rcosx=Math.cos(RangX * PIh),Rsinxp=Math.sin(RangX * PIh),Rsinxn=Math.sin(-RangX * PIh),
  Rcosy=Math.cos(RangY * PIh),Rsinyp=Math.sin(RangY * PIh),Rsinyn=Math.sin(-RangY * PIh),
  R1=Rcosy,R2=Rsinxp*Rsinyp,R3=Rcosx*Rsinyp,R4=0,
  R5=0,R6=Rcosx,R7=Rsinxn,R8=0,
  R9=Rsinyn,R10=Rsinxp*Rcosy,R11=Rcosx*Rcosy,R12=0,R16=1;  
  r1 = [
    [sclx, 0,   0,  0],
    [0,  scly,  0,  0],
    [0,  0,   sclz, 0],
    [0,  0,   0,  1]];
  r2 = [
    [1,  0,  0, 0],
    [0,  Rcosx,  Rsinxn, 0],
    [0,  Rsinxp, Rcosx, 0],
    [0,  0,   0,   1]];
  r3 = [
    [Rcosy,  Rsinyn, 0,   0],
    [Rsinyp,  Rcosy,  0,   0],
    [0,     0,     0,   0],
    [x,     y,     rotationZ,   1]];
var r1row = r1.length, 
  r1col = r1[0].length, 
  r2col = r2[0].length,
  r3col = r3[0].length,
    protoR = new Array(r1row);
  for (var r = 0; r < r1row; ++r) {
    protoR[r] = new Array(r2col); 
    for (var c = 0; c < r2col; ++c) {
      protoR[r][c] = 0;
      for (var i = 0; i < r1col; ++i) {
        protoR[r][c] += r1[r][i] * r2[i][c];
      }
    }
  }
 var protoRrow = protoR.length, 
     protoRcol = protoR[0].length, 
      ronMatrix = new Array(protoRrow);
  for (var r = 0; r < protoRrow; ++r) {
    ronMatrix[r] = new Array(r3col); 
    for (var c = 0; c < r3col; ++c) {
      ronMatrix[r][c] = 0;
      for (var i = 0; i < protoRcol; ++i) {
        ronMatrix[r][c] += protoR[r][i] * r3[i][c];
      }
    }
  }


function matrixArrayToCssMatrixRON(array) { return "matrix3d(" + array.join(',') + ")";}
var matrixRON = matrixArrayToCssMatrixRON( protoR );



/*MDT.matrixArrayToCssMatrixRON = function (array) {return "matrix3d(" + array.join(',') + ")";}
 var matrixRON = MDT.matrixArrayToCssMatrixRON( ronMatrix );*/
///////////////////////////////////// Obj Movement Tri
var objID = e.data.objID;
//var proto = e.data.proto;  
var X = e.data.X; 
var Y = e.data.Y; 
var Z = e.data.Z; 
var SX = e.data.SX; 
var SY = e.data.SY; 
var SZ = e.data.SZ; 
var Rx = e.data.Rx; 
var Ry = e.data.Ry; 
var rt = e.data.rt; 
var objMatrix = [];
var proto = [];
var angX=Rx,angY=Ry,angZ=rt,
  cosz=Math.cos(angZ * PIh),sinzp=Math.sin(angZ * PIh),sinzn=Math.sin(-angZ * PIh),
  cosx=Math.cos(angX * PIh),sinxp=Math.sin(angX * PIh),sinxn=Math.sin(-angX * PIh),
  cosy=Math.cos(angY * PIh),sinyp=Math.sin(angY * PIh),sinyn=Math.sin(-angY * PIh),
  a1=cosy,a2=sinxp*sinyp,a3=cosx*sinyp,a4=0,
  a5=0,a6=cosx,a7=sinxn,a8=0,
  a9=sinyn,a10=sinxp*cosy,a11=cosx*cosy,a12=0,
  a13=X,a14=Y,a15=Z,a16=1;  
  m1 = [
    [SX, 0,   0,  0],
    [0,  SY,  0,  0],
    [0,  0,   SZ, 0],
    [X,  Y,   Z,  1]];
  m2 = [
    [a1,  a2,  a3, a4],
    [a5,  a6,  a7, a8],
    [a9, a10, a11, a12],
    [0,  0,   0,   a16]];
  m3 = [
    [cosz,  sinzn, 0,   0],
    [sinzp, cosz,  0,   0],
    [0,     0,     1,   0],
    [0,     0,     0,   a16]];
var m1row = m1.length, 
  m1col = m1[0].length, 
  m2col = m2[0].length,
  m3col = m3[0].length,
    proto = new Array(m1row);
  for (var r = 0; r < m1row; ++r) {
    proto[r] = new Array(m2col); 
    for (var c = 0; c < m2col; ++c) {
      proto[r][c] = 0;
      for (var i = 0; i < m1col; ++i) {
        proto[r][c] += m1[r][i] * m2[i][c];
      }
    }
  }
 var protorow = proto.length, 
     protocol = proto[0].length, 
      objMatrix = new Array(protorow);
  for (var r = 0; r < protorow; ++r) {
    objMatrix[r] = new Array(m3col); 
    for (var c = 0; c < m3col; ++c) {
      objMatrix[r][c] = 0;
      for (var i = 0; i < protocol; ++i) {
        objMatrix[r][c] += proto[r][i] * m3[i][c];
      }
    }
  }

function matrixArrayToCssMatrixOBJ(array) { return "matrix3d(" + array.join(',') + ")";}
 var matrixToCSS = matrixArrayToCssMatrixOBJ( objMatrix );





  var workermatrix = ({"Id": clients.length, "obj":objID, Matrix: matrixToCSS, M1:m1, M2:m2, M3:m3, MatrixRon: matrixRON, r1:r1,r2:r2,r3:r3, Ysub:Ysub, Yadd:Yadd, Xsub:Xsub, Xadd:Xadd, XYco:XYco, YXco:YXco, XYfo:XYfo, YXfo:YXfo, XYfp:XYfp, YXfp:YXfp, x:x, y:y, YcursoraxisX:YcursoraxisX,XcursoraxisY:XcursoraxisY,floor1Y:floor1Y,floor1X:floor1X,rotationX:rotationX,rotationY:rotationY,YplayeraxisX:YplayeraxisX,XplayeraxisY:XplayeraxisY});


//port.postMessage(workermatrix);
  broadcast(clients, workermatrix);


  self.postMessage({
    mat: msg.data.workermatrix,
    numPrimes: numPrimes,
    offset: msg.data.offset,
    length: msg.data.length
  })

//}, refresh);
/////////////////////////////////////////////////////////////////
    })

    port.start();
/////////////////////////////////////////////////////////////////

    broadcast(clients, {"id": clients.length, "cmd": "connected"});
    self.onoffline = function() {
console.log('Your worker is now offline');
}

  }, false);