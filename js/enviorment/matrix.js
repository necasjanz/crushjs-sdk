self.addEventListener('message', function(e) {
var data = e.data;
var refresh = e.data.refresh;
var objID = e.data.objID;
var proto = e.data.proto;  
var X = e.data.X; 
var Y = e.data.Y; 
var Z = e.data.Z; 
var SX = e.data.SX; 
var SY = e.data.SY; 
var SZ = e.data.SZ; 
var Rx = e.data.Rx; 
var Ry = e.data.Ry; 
var rt = e.data.rt; 
var MDN = MDN || {};
var objMatrix = [];
var proto = [];
var newmatrix = [];
var PIh=Math.PI / 180;
var angX=Rx,angY=Ry,angZ=rt,
	cosz=Math.cos(angZ * PIh),sinzp=Math.sin(angZ * PIh),sinzn=Math.sin(-angZ * PIh),
 	cosx=Math.cos(angX * PIh),sinxp=Math.sin(angX * PIh),sinxn=Math.sin(-angX * PIh),
	cosy=Math.cos(angY * PIh),sinyp=Math.sin(angY * PIh),sinyn=Math.sin(-angY * PIh),
	a1=cosy,a2=sinxp*sinyp,a3=cosx*sinyp,a4=0,
	a5=0,a6=cosx,a7=sinxn,a8=0,
	a9=sinyn,a10=sinxp*cosy,a11=cosx*cosy,a12=0,
	a13=X,a14=Y,a15=Z,a16=1;	
	m1 = [
    [SX, 0,   0,  0],
    [0,  SY,  0,  0],
    [0,  0,   SZ, 0],
    [X,  Y,   Z,  1]];
	m2 = [
    [a1,  a2,  a3, a4],
    [a5,  a6,  a7, a8],
    [a9, a10, a11, a12],
    [0,  0,   0,   a16]];
  m3 = [
    [cosz,  sinzn, 0,   0],
    [sinzp, cosz,  0,   0],
    [0,     0,     1,   0],
    [0,     0,     0,   a16]];
var m1row = m1.length, 
	m1col = m1[0].length, 
	m2col = m2[0].length,
	m3col = m3[0].length,
    proto = new Array(m1row);
  for (var r = 0; r < m1row; ++r) {
    proto[r] = new Array(m2col); 
    for (var c = 0; c < m2col; ++c) {
      proto[r][c] = 0;
      for (var i = 0; i < m1col; ++i) {
        proto[r][c] += m1[r][i] * m2[i][c];
      }
    }
  }
 var protorow = proto.length, 
 	   protocol = proto[0].length, 
      objMatrix = new Array(protorow);
  for (var r = 0; r < protorow; ++r) {
    objMatrix[r] = new Array(m3col); 
    for (var c = 0; c < m3col; ++c) {
      objMatrix[r][c] = 0;
      for (var i = 0; i < protocol; ++i) {
        objMatrix[r][c] += proto[r][i] * m3[i][c];
      }
    }
  }
  MDN.matrixArrayToCssMatrix = function (array) {return "matrix3d(" + array.join(',') + ")";}
  var matrixToCSS = MDN.matrixArrayToCssMatrix( objMatrix );
  var workermatrix = ({Matrix: matrixToCSS});
  //console.log(matrixToCSS)
self.postMessage(workermatrix);
    }, false);
//Autor: Manuel Janz Pereira necasjanz@gmail.com