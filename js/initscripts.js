function assets() {
"use strict";
};
function initScripts() {
"use strict";
//SDK

translate();
rotate();
scale();
//skew();
//objprep();
BGcolor();
BGmask();
FGpoly();

creatList();
handle0();
handle1();
handle2();


OBJFindH();
updateAllH();
ativ();

X=0,Y=0,Z=0;

}
//SDK assets
onDOMContentLoaded = (function(){ initScripts();console.log("DOM ready!") })()
//Game assets
onloadeddata = (function(){assets(); console.log("Data loaded!") })()
onload = (function(){requestAnimationFrame(draw);
console.log("Page fully loaded!") })()
//Copyright (C) 2018 Manuel Janz Pereira
//Autor: Manuel Janz Pereira necasjanz@gmail.com