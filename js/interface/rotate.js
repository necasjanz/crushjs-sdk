function rotate() {
const dr0 = document.createElement("div");dr0.setAttribute("id", "rotateWin");dr0.setAttribute("ondragstart", "dragStart(event)");dr0.setAttribute("draggable", "draggable='true'");dr0.style.position = 'absolute',dr0.style.width = '210px',dr0.style.height = '220px',dr0.style.left = '330px',dr0.style.top = '120px',dr0.style.zIndex = '90',dr0.style.backgroundColor = uiBK,dr0.style.textAlign = 'center',dr0.style.border = '1px solid #d3d3d3',dr0.style.display = 'none';
const drh = document.createElement("div");drh.setAttribute("id", "rotateWinheader");drh.style.padding = '10px',drh.style.cursor = 'move',drh.style.zIndex = '100',drh.style.backgroundColor = uiHD,drh.style.color = '#fff';
const dim0 = document.createElement("div");dim0.setAttribute("id", "toolsRotate");dim0.style.color = 'white',dim0.style.width = '100%',dim0.style.height = '100%',dim0.style.left = '0%',dim0.style.top = '20%',dim0.style.overflow = 'hidden',dim0.style.position = 'absolute';
const ta0 = document.createElement("TABLE");ta0.setAttribute("id", "TBsphere");const tr0 = document.createElement("TR");tr0.setAttribute("style", "border:1px");const td0 = document.createElement("TD");const td1 = document.createElement("TD");const td2 = document.createElement("TD");
const i0 = document.createElement("INPUT");i0.setAttribute("type", "range");i0.setAttribute("min", "0");i0.setAttribute("value", "0");i0.setAttribute("max", "360");i0.setAttribute("id", "R");i0.setAttribute("class", "slider");i0.style.width='100%',i0.style.height='5px',i0.style.borderRadius='2.5px',i0.style.background=Xcolor,i0.style.outline='none';
const ch0 = document.createElement("INPUT");ch0.setAttribute("type", "checkbox");ch0.onclick = function() { if (ch0.checked == true){actRnX = 1;} else {actRnX = 0;} };
const bt0 = document.createElement("BUTTON");bt0.onclick = function() { changeR();};
const tr1 = document.createElement("TR");tr1.setAttribute("style", "border:1px");const td3 = document.createElement("TD");const td4 = document.createElement("TD");const td5 = document.createElement("TD");
const i1 = document.createElement("INPUT");i1.setAttribute("min", "0");i1.setAttribute("value", "0");i1.setAttribute("max", "360");i1.setAttribute("type", "range");i1.setAttribute("id", "RX");i1.setAttribute("class", "slider");i1.style.width='100%',i1.style.height='5px',i1.style.borderRadius='2.5px',i1.style.background=Ycolor,i1.style.outline='none';
const ch1 = document.createElement("INPUT");ch1.setAttribute("type", "checkbox");ch1.onclick = function() { if (ch1.checked == true){actRnY = 1;} else {actRnY = 0;} };
const bt1 = document.createElement("BUTTON");bt1.onclick = function() { changeRx();};
const tr2 = document.createElement("TR");tr2.setAttribute("style", "border:1px");const td6 = document.createElement("TD");const td7 = document.createElement("TD");const td8 = document.createElement("TD");
const i2 = document.createElement("INPUT");i2.setAttribute("min", "0");i2.setAttribute("value", "0");i2.setAttribute("max", "360");i2.setAttribute("type", "range");i2.setAttribute("id", "RY");i2.setAttribute("class", "slider");i2.style.width='100%',i2.style.height='5px',i2.style.borderRadius='2.5px',i2.style.background=Zcolor,i2.style.outline='none';
const ch2 = document.createElement("INPUT");ch2.setAttribute("type", "checkbox");ch2.onclick = function() { if (ch2.checked == true){actRnZ = 1;} else {actRnZ = 0;} };
const bt2 = document.createElement("BUTTON");bt2.onclick = function() { changeRy();};
const tr3 = document.createElement("TR");tr3.setAttribute("style", "border:1px");const td9 = document.createElement("TD");const td10 = document.createElement("TD");const td11 = document.createElement("TD");
const i3 = document.createElement("INPUT");i3.setAttribute("min", "0");i3.setAttribute("value", "0");i3.setAttribute("max", "360");i3.setAttribute("type", "range");i3.setAttribute("id", "RX3d");i3.setAttribute("class", "slider");i3.style.width='100%',i3.style.height='5px',i3.style.borderRadius='2.5px',i3.style.background=Xcolor,i3.style.outline='none';
const ch3 = document.createElement("INPUT");ch3.setAttribute("type", "checkbox");ch3.onclick = function() { if (ch3.checked == true){actRX = 1;} else {actRX = 0;} };
const bt3 = document.createElement("BUTTON");bt3.onclick = function() { changeRx3d();};
const tr4 = document.createElement("TR");tr4.setAttribute("style", "border:1px");const td12 = document.createElement("TD");const td13 = document.createElement("TD");const td14 = document.createElement("TD");
const i4 = document.createElement("INPUT");i4.setAttribute("min", "0");i4.setAttribute("value", "0");i4.setAttribute("max", "360");i4.setAttribute("type", "range");i4.setAttribute("id", "RY3d");i4.setAttribute("class", "slider");i4.style.width='100%',i4.style.height='5px',i4.style.borderRadius='2.5px',i4.style.background=Ycolor,i4.style.outline='none';
const ch4 = document.createElement("INPUT");ch4.setAttribute("type", "checkbox");ch4.onclick = function() { if (ch4.checked == true){actRY = 1;} else {actRY = 0;} };
const bt4 = document.createElement("BUTTON"); bt4.onclick = function() { changeRy3d();};
const tr5 = document.createElement("TR");tr5.setAttribute("style", "border:1px");const td15 = document.createElement("TD");const td16 = document.createElement("TD");const td17 = document.createElement("TD");
const i5 = document.createElement("INPUT");i5.setAttribute("min", "0");i5.setAttribute("value", "0");i5.setAttribute("max", "360");i5.setAttribute("type", "range");i5.setAttribute("id", "RZ3d");i5.setAttribute("class", "slider");i5.style.width='100%',i5.style.height='5px',i5.style.borderRadius='2.5px',i5.style.background=Zcolor,i5.style.outline='none';
const ch5 = document.createElement("INPUT");ch5.setAttribute("type", "checkbox");ch5.onclick = function() { if (ch5.checked == true){actRZ = 1;} else {actRZ = 0;} };
const bt5 = document.createElement("BUTTON");bt5.onclick = function() { changeRz3d();};
const tr6 = document.createElement("TR");tr6.setAttribute("style", "border:1px");const td18 = document.createElement("TD");const td19 = document.createElement("TD");const td20 = document.createElement("TD");
const i6 = document.createElement("INPUT");i6.setAttribute("min", "0");i6.setAttribute("value", "0");i6.setAttribute("max", "360");i6.setAttribute("type", "range");i6.setAttribute("id", "AG3d");i6.setAttribute("class", "slider");i6.style.width='100%',i6.style.height='5px',i6.style.borderRadius='2.5px',i6.style.background='orange',i6.style.outline='none';
const ch6 = document.createElement("INPUT");ch6.setAttribute("type", "checkbox");ch6.onclick = function() { if (ch6.checked == true){actRA = 1;} else {actRA = 0;} };
const bt6 = document.createElement("BUTTON");bt6.onclick = function() { changeRa3d();};
document.getElementById("frame").append(dr0);dr0.append(drh);dr0.append(dim0);dim0.appendChild(ta0);
ta0.appendChild(tr0);ta0.appendChild(tr1);ta0.appendChild(tr2);ta0.appendChild(tr3);ta0.appendChild(tr4);ta0.appendChild(tr5);ta0.appendChild(tr6);
tr0.appendChild(td0);tr0.appendChild(td1);tr0.appendChild(td2);td0.appendChild(i0);td1.appendChild(ch0);td2.appendChild(bt0);
tr1.appendChild(td3);tr1.appendChild(td4);tr1.appendChild(td5);td3.appendChild(i1);td4.appendChild(ch1);td5.appendChild(bt1);
tr2.appendChild(td6);tr2.appendChild(td7);tr2.appendChild(td8);td6.appendChild(i2);td7.appendChild(ch2);td8.appendChild(bt2);
tr3.appendChild(td9);tr3.appendChild(td10);tr3.appendChild(td11);td9.appendChild(i3);td10.appendChild(ch3);td11.appendChild(bt3);
tr4.appendChild(td12);tr4.appendChild(td13);tr4.appendChild(td14);td12.appendChild(i4);td13.appendChild(ch4);td14.appendChild(bt4);
tr5.appendChild(td15);tr5.appendChild(td16);tr5.appendChild(td17);td15.appendChild(i5);td16.appendChild(ch5);td17.appendChild(bt5);
tr6.appendChild(td18);tr6.appendChild(td19);tr6.appendChild(td20);td18.appendChild(i6);td19.appendChild(ch6);td20.appendChild(bt6);
bt0.innerHTML = i0.value;i0.oninput = function() {  bt0.innerHTML = this.value;rt = bt0.innerHTML;}
bt1.innerHTML = i1.value;i1.oninput = function() {  bt1.innerHTML = this.value;Rx = bt1.innerHTML;}
bt2.innerHTML = i2.value;i2.oninput = function() {  bt2.innerHTML = this.value;Ry = bt2.innerHTML;}
bt3.innerHTML = i3.value;i3.oninput = function() {  bt3.innerHTML = this.value;Rx3d = bt3.innerHTML;}
bt4.innerHTML = i4.value;i4.oninput = function() {  bt4.innerHTML = this.value;Ry3d = bt4.innerHTML;}
bt5.innerHTML = i5.value;i5.oninput = function() {  bt5.innerHTML = this.value;rt3d = bt5.innerHTML;}
bt6.innerHTML = i6.value;i6.oninput = function() {  bt6.innerHTML = this.value;rtA = bt6.innerHTML;}
function changeR() { var txt; var person = prompt("Enter X rotation:"); if (person == null || person == "") { txt = "User cancelled the prompt."; } else { txt =  person; } actRnX = 1; rt = txt;bt0.innerHTML = txt;i0.value = txt;}
function changeRx() { var txt; var person = prompt("Enter Y rotation:"); if (person == null || person == "") { txt = "User cancelled the prompt.";  } else { txt =  person; } actRnY = 1; Rx = txt;bt1.innerHTML = txt;i1.value = txt;}
function changeRy() { var txt; var person = prompt("Enter Z rotation:"); if (person == null || person == "") { txt = "User cancelled the prompt."; } else { txt =  person; } actRnX = 1; Ry = txt;bt2.innerHTML = txt;i2.value = txt;}
function changeRx3d() { var txt; var person = prompt("Enter Y rotation:"); if (person == null || person == "") { txt = "User cancelled the prompt.";  } else { txt =  person; } actRX = 1; Rx3d = txt;bt3.innerHTML = txt;i3.value = txt;}
function changeRy3d() { var txt; var person = prompt("Enter Z rotation:"); if (person == null || person == "") { txt = "User cancelled the prompt."; } else { txt =  person; } actRX = 1; Ry3d = txt;bt4.innerHTML = txt;i4.value = txt;}
function changeRz3d() { var txt; var person = prompt("Enter X rotation:"); if (person == null || person == "") { txt = "User cancelled the prompt."; } else { txt =  person; } actRA = 1; rt3d = txt;bt5.innerHTML = txt;i5.value = txt;}
function changeRa3d() { var txt; var person = prompt("Enter Z rotation:"); if (person == null || person == "") { txt = "User cancelled the prompt."; } else { txt =  person; } actRnX = 1; rtA = txt;bt6.innerHTML = txt;i6.value = txt;}
dragElement(document.getElementById(("rotateWin")));}
//Copyright (C) 2018 Manuel Janz Pereira
//Autor: Manuel Janz Pereira necasjanz@gmail.com