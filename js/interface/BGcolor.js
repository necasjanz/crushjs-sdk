function BGcolor() {
const dc0 = document.createElement("div");dc0.setAttribute("id", "BGcolorWin");dc0.setAttribute("ondragstart", "dragStart(event)");dc0.setAttribute("draggable", "draggable='true'");dc0.style.position = 'absolute',dc0.style.width = '250px',dc0.style.height = '140px',dc0.style.left = '360px',dc0.style.top = '140px',dc0.style.zIndex = '90',dc0.style.backgroundColor = uiBK,dc0.style.textAlign = 'center',dc0.style.border = '1px solid #d3d3d3',dc0.style.display = 'none';
const dch = document.createElement("div");dch.setAttribute("id", "BGcolorWinheader");dch.style.padding = '10px',dch.style.cursor = 'move',dch.style.zIndex = '100',dch.style.backgroundColor = uiHD,dch.style.color = '#fff';
const dim0 = document.createElement("div");dim0.setAttribute("id", "toolsScale");dim0.style.color = 'white',dim0.style.width = '100%',dim0.style.height = '100%',dim0.style.left = '0%',dim0.style.top = '20%',dim0.style.overflow = 'overflow',dim0.style.position = 'absolute';
const ta0 = document.createElement("TABLE");ta0.setAttribute("id", "TBsphere");
const tr0 = document.createElement("TR");tr0.setAttribute("style", "border:1px");
const td0 = document.createElement("TD");const td1 = document.createElement("TD");const td2 = document.createElement("TD");const td3 = document.createElement("TD");
const i0 = document.createElement("INPUT");i0.setAttribute("type", "range");i0.setAttribute("min", "0");i0.setAttribute("value", "0");i0.setAttribute("max", "255");i0.setAttribute("class", "slider");
const tn0 = document.createTextNode("Red:");
const bt0 = document.createElement("BUTTON");
const tr1 = document.createElement("TR");tr1.setAttribute("style", "border:1px");
const td4 = document.createElement("TD");const td5= document.createElement("TD");const td6 = document.createElement("TD");
const i1 = document.createElement("INPUT");i1.setAttribute("min", "0");i1.setAttribute("value", "0");i1.setAttribute("max", "255");i1.setAttribute("type", "range");i1.setAttribute("class", "slider");
const tn1 = document.createTextNode("Green:");
const bt1 = document.createElement("BUTTON");  
const tr2 = document.createElement("TR");tr2.setAttribute("style", "border:1px");
const td7 = document.createElement("TD");const td8 = document.createElement("TD");const td9 = document.createElement("TD");
const i2 = document.createElement("INPUT");i2.setAttribute("min", "0");i2.setAttribute("value", "0");i2.setAttribute("max", "255");i2.setAttribute("type", "range");i2.setAttribute("class", "slider");
const tn2 = document.createTextNode("Blue:");
const bt2 = document.createElement("BUTTON");
const tr3 = document.createElement("TR");tr3.setAttribute("style", "border:1px");
const td10 = document.createElement("TD");const td11 = document.createElement("TD");const td12 = document.createElement("TD");
const i3 = document.createElement("INPUT");i3.setAttribute("min", "0");i3.setAttribute("value", "255");i3.setAttribute("max", "255");i3.setAttribute("type", "range");i3.setAttribute("class", "slider");
const tn3 = document.createTextNode("Opacity:");
const bt3 = document.createElement("BUTTON"); 
document.getElementById("frame").append(dc0);dc0.append(dim0);dc0.append(dch);dim0.appendChild(ta0);
ta0.appendChild(tr0);ta0.appendChild(tr1);ta0.appendChild(tr2);ta0.appendChild(tr3);
tr0.appendChild(td1);tr0.appendChild(td2);tr0.appendChild(td3);td1.appendChild(i0);td2.appendChild(bt0);td3.appendChild(tn0);
tr1.appendChild(td4);tr1.appendChild(td5);tr1.appendChild(td6);td4.appendChild(i1);td5.appendChild(bt1);td6.appendChild(tn1);
tr2.appendChild(td7);tr2.appendChild(td8);tr2.appendChild(td9);td7.appendChild(i2);td8.appendChild(bt2);td9.appendChild(tn2);
tr3.appendChild(td10);tr3.appendChild(td11);tr3.appendChild(td12);td10.appendChild(i3);td11.appendChild(bt3);td12.appendChild(tn3);
bt0.innerHTML = i0.value;i0.oninput = function() {  bt0.innerHTML = this.value;SBBred = bt0.innerHTML;}
bt1.innerHTML = i1.value;i1.oninput = function() {  bt1.innerHTML = this.value;SBBgreen = bt1.innerHTML;}
bt2.innerHTML = i2.value;i2.oninput = function() {  bt2.innerHTML = this.value;SBBblue = bt2.innerHTML;}//SBBopacity
bt3.innerHTML = i3.value;i3.oninput = function() {  bt3.innerHTML = this.value;SBBopacity = bt3.innerHTML;}
bt0.onclick = function() {  var txt; var person = prompt("Enter X scale:"); if (person == null || person == "") { txt = "User cancelled the prompt."; } else { txt =  person; } SBBred = txt;bt0.innerHTML = txt;i0.value = txt;}
bt1.onclick = function() {  var txt; var person = prompt("Enter Y scale:"); if (person == null || person == "") { txt = "User cancelled the prompt.";  } else { txt =  person; } SBBgreen = txt;bt1.innerHTML = txt;i1.value = txt;}
bt2.onclick = function() {  var txt; var person = prompt("Enter Z scale:"); if (person == null || person == "") { txt = "User cancelled the prompt."; } else { txt =  person; } SBBblue = txt;bt2.innerHTML = txt;i2.value = txt;}
bt3.onclick = function() {  var txt; var person = prompt("Enter Z scale:"); if (person == null || person == "") { txt = "User cancelled the prompt."; } else { txt =  person; } SBBopacity = txt;bt3.innerHTML = txt;i3.value = txt;}
dragElement(document.getElementById(("BGcolorWin")));
}
//Copyright (C) 2018 Manuel Janz Pereira
//Autor: Manuel Janz Pereira necasjanz@gmail.com