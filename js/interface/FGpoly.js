let vertice,px1 = '0', py1 = '0', vector = [],verticX,verticY,i6,i7,i8,i9,thc,sg0,g,po,pat,img;
function FGpoly() {
svgNS = "http://www.w3.org/2000/svg";
const dp0 = document.createElement("div");dp0.setAttribute("id", "FGpolyWin");dp0.setAttribute("ondragstart", "dragStart(event)");dp0.setAttribute("draggable", "draggable='true'");dp0.style.position = 'absolute',dp0.style.width = '250px',dp0.style.height = '555px',dp0.style.left = '330px',dp0.style.top = '120px',dp0.style.zIndex = '90',dp0.style.backgroundColor = '#70798A',dp0.style.textAlign = 'center',dp0.style.border = '1px solid #d3d3d3',dp0.style.display = 'none';
const dph = document.createElement("div");dph.setAttribute("id", "FGpolyWinheader");dph.style.padding = '10px',dph.style.cursor = 'move',dph.style.zIndex = '100',dph.style.backgroundColor = uiHD,dph.style.color = '#fff';
const dim0 = document.createElement("div");dim0.setAttribute("id", "EditSolidVertPolygond");dim0.style.color = 'white',dim0.style.width = '250px',dim0.style.height = '100%',dim0.style.left = '0%',dim0.style.top = '4%',dim0.style.overflow = 'hidden',dim0.style.position = 'absolute',dim0.style.backgroundColor = uiBK;
const ta0 = document.createElement("TABLE");ta0.setAttribute("id", "TBsphere");
const tr0 = document.createElement("TR");tr0.setAttribute("style", "border:1px");
const td0 = document.createElement("td");td0.setAttribute("style", "width: 50%;");
const bt0 = document.createElement("BUTTON");bt0.setAttribute("id", "addV");bt0.onclick = function() { addVertice(); };
const tn0 = document.createTextNode("Add Vertice");
const td1 = document.createElement("TD");
const bt1 = document.createElement("BUTTON");bt1.setAttribute("id", "rmV");bt1.onclick = function() { rmVertice(); };
const tn1 = document.createTextNode("Remove Vertice");
const td2 = document.createElement("TD");
const tn2 = document.createTextNode("X:");
const td3 = document.createElement("TD");
const bt2 = document.createElement("BUTTON");bt2.setAttribute("id", "polypx1out");//bt2.onclick = function() { addVertice(); };
const td4 = document.createElement("TD");
const tn3 = document.createTextNode("Y:");
const td5 = document.createElement("TD");
const bt3 = document.createElement("BUTTON");bt3.setAttribute("id", "polypy1out");//bt3.onclick = function() { addVertice(); };
const ta1 = document.createElement("TABLE");ta1.setAttribute("id", "TBsphere");
const tr1 = document.createElement("TR");tr1.setAttribute("style", "border:1px");
const td6 = document.createElement("TD");td6.setAttribute("style", "width: 100%;");
const di0 = document.createElement("div");di0.setAttribute("id", "slidecontainer12");
const i0 = document.createElement("INPUT");i0.setAttribute("min", "0");i0.setAttribute("value", "100");i0.setAttribute("max", "1000");i0.setAttribute("type", "range");i0.setAttribute("class", "slider");i0.setAttribute("id", "polypx1");
const di1 = document.createElement("div");di1.setAttribute("class", "slider-wrapper");
const i1 = document.createElement("INPUT");i1.setAttribute("min", "0");i1.setAttribute("value", "100");i1.setAttribute("max", "1000");i1.setAttribute("type", "range");i1.setAttribute("class", "slider");i1.setAttribute("id", "polypy1");
sg0 = document.createElementNS (svgNS, "svg");sg0.setAttributeNS (null, "width", "220px");sg0.setAttributeNS (null, "height", "220px");sg0.style.display = "block";sg0.style.marginLeft='15px';sg0.style.marginTop='40px';sg0.style.left="0px";sg0.style.top='13px';sg0.style.position='absolute';sg0.style.backgroundColor='#1E57A5';
g = document.createElementNS(svgNS, "g");
po = document.createElementNS(svgNS,"polygon");po.setAttributeNS(null, "points", ""+vectorformula+"");po.style = "fill='url(#img1)'";
var def = document.createElementNS(svgNS,"defs");
pat = document.createElementNS(svgNS,"pattern");pat.setAttributeNS(null,"id","img1");pat.setAttributeNS(null,"patternUnits","userSpaceOnUse");pat.setAttributeNS(null,"width","100%");pat.setAttributeNS(null,"height","100%");
img = document.createElementNS(svgNS,"image");img.setAttributeNS(null,"id","imgP");img.setAttributeNS(null,"href","images/Q3cUg29.gif");
thc = document.createElement("div");thc.style.width = '220px',thc.style.height = '220px',thc.style.marginLeft='5px';thc.style.marginTop='40px';thc.style.left="10px";thc.style.top='13px';thc.style.position='absolute';thc.style.backgroundColor='';thc.style.opacity='0';thc.style.zIndex='10';
const di2 = document.createElement("div");di2.setAttribute("class", "spheretoolsOrigin");
const sl0 = document.createElement("SELECT");sl0.setAttribute("id", "vector");sl0.setAttribute("onchange", "selectOptionV();");sl0.setAttribute("size", "4");sl0.style.width="250px";sl0.style.height="100px";
const btimg0 = document.createElement("BUTTON");btimg0.onclick = function() { polymask=1;  
td7.replaceChild(tn8,tn4); td8.replaceChild(i6,i2);td9.replaceChild(bt8,bt4);
td10.replaceChild(tn9,tn5);td11.replaceChild(i7,i3);td12.replaceChild(bt9,bt5);
td13.replaceChild(tn10,tn6);td14.replaceChild(i8,i4);td15.replaceChild(bt10,bt6);
td16.replaceChild(tn11,tn7);td17.replaceChild(i9,i5);td18.replaceChild(bt11,bt7);
};
const tnimg0 = document.createTextNode("Texture");
const btcol0 = document.createElement("BUTTON");btcol0.onclick = function() { polymask=0;  
td7.replaceChild(tn4,tn8); td8.replaceChild(i2,i6);td9.replaceChild(bt4,bt8); 
td10.replaceChild(tn5,tn9);td11.replaceChild(i3,i7);td12.replaceChild(bt5,bt9);
td13.replaceChild(tn6,tn10);td14.replaceChild(i4,i8);td15.replaceChild(bt6,bt10);
td16.replaceChild(tn7,tn11);td17.replaceChild(i5,i9);td18.replaceChild(bt7,bt11);
};
const tnicol0 = document.createTextNode("Fill");
const ta2 = document.createElement("TABLE");ta2.setAttribute("id", "TBsphere");
const tr2 = document.createElement("TR");tr2.setAttribute("style", "border:1px");
const td7 = document.createElement("TD");td7.setAttribute("style", "width: 10%;");const td8 = document.createElement("TD");td8.setAttribute("style", "width: 80%;");const td9 = document.createElement("TD");td9.setAttribute("style", "width: 100%;");
tn4 = document.createTextNode("Red:");
const i2 = document.createElement("INPUT");i2.setAttribute("min", "0");i2.setAttribute("value", "100");i2.setAttribute("max", "255");i2.setAttribute("type", "range");i2.setAttribute("class", "slider");i2.setAttribute("id", "PolyBGred");
const bt4 = document.createElement("BUTTON");bt4.setAttribute("id", "redoutPolyBG");
const ta3 = document.createElement("TABLE");ta3.setAttribute("id", "TBsphere");
const tr3 = document.createElement("TR");tr3.setAttribute("style", "border:1px");
const td10 = document.createElement("TD");td10.setAttribute("style", "width: 10%;");const td11 = document.createElement("TD");td11.setAttribute("style", "width: 80%;");const td12 = document.createElement("TD");td12.setAttribute("style", "width: 100%;");
const tn5 = document.createTextNode("Green:");
const i3 = document.createElement("INPUT");i3.setAttribute("min", "0");i3.setAttribute("value", "100");i3.setAttribute("max", "255");i3.setAttribute("type", "range");i3.setAttribute("class", "slider");i3.setAttribute("id", "PolyBGgreen");
const bt5 = document.createElement("BUTTON");bt5.setAttribute("id", "greenoutPolyBG");
const ta4 = document.createElement("TABLE");ta4.setAttribute("id", "TBsphere");
const tr4 = document.createElement("TR");tr4.setAttribute("style", "border:1px");
const td13 = document.createElement("TD");td13.setAttribute("style", "width: 10%;");const td14 = document.createElement("TD");td14.setAttribute("style", "width: 80%;");const td15 = document.createElement("TD");td15.setAttribute("style", "width: 10%;");
const tn6 = document.createTextNode("Blue:");
const i4 = document.createElement("INPUT");i4.setAttribute("min", "0");i4.setAttribute("value", "100");i4.setAttribute("max", "255");i4.setAttribute("type", "range");i4.setAttribute("class", "slider");i4.setAttribute("id", "PolyBGblue");
const bt6 = document.createElement("BUTTON");bt6.setAttribute("id", "blueoutPolyBG");
const ta5 = document.createElement("TABLE");ta5.setAttribute("id", "TBsphere");
const tr5 = document.createElement("TR");tr5.setAttribute("style", "border:1px");
const td16 = document.createElement("TD");td16.setAttribute("style", "width: 10%;");const td17 = document.createElement("TD");td17.setAttribute("style", "width: 80%;");const td18 = document.createElement("TD");td18.setAttribute("style", "width: 100%;");
const tn7 = document.createTextNode("Opacity:");
const i5 = document.createElement("INPUT");i5.setAttribute("min", "0");i5.setAttribute("value", "100");i5.setAttribute("max", "100");i5.setAttribute("type", "range");i5.setAttribute("class", "slider");i5.setAttribute("id", "PolyBGopacity");
const bt7 = document.createElement("BUTTON");bt7.setAttribute("id", "opacityoutPolyBG");
const ta6 = document.createElement("TABLE");ta6.setAttribute("id", "TBsphere");
const tr6 = document.createElement("TR");tr6.setAttribute("style", "border:1px");
const td19 = document.createElement("TD");td19.setAttribute("style", "width: 10%;");const td20 = document.createElement("TD");td20.setAttribute("style", "width: 80%;");const td21 = document.createElement("TD");td21.setAttribute("style", "width: 100%;");
const tn8 = document.createTextNode("ImgHeight:");
 i6 = document.createElement("INPUT");i6.setAttribute("min", "0");i6.setAttribute("value", "100");i6.setAttribute("max", "1000");i6.setAttribute("type", "range");i6.setAttribute("class", "slider");i6.setAttribute("id", "PolyBGopacity");
const bt8 = document.createElement("BUTTON");bt8.setAttribute("id", "opacityoutPolyBG");
const ta7 = document.createElement("TABLE");ta7.setAttribute("id", "TBsphere");
const tr7 = document.createElement("TR");tr7.setAttribute("style", "border:1px");
const td22 = document.createElement("TD");td22.setAttribute("style", "width: 10%;");const td23 = document.createElement("TD");td23.setAttribute("style", "width: 80%;");const td24 = document.createElement("TD");td24.setAttribute("style", "width: 100%;");
const tn9 = document.createTextNode("ImgWidth:");
 i7 = document.createElement("INPUT");i7.setAttribute("min", "0");i7.setAttribute("value", "100");i7.setAttribute("max", "1000");i7.setAttribute("type", "range");i7.setAttribute("class", "slider");i7.setAttribute("id", "PolyBGopacity");
const bt9 = document.createElement("BUTTON");//bt9.setAttribute("id", "opacityoutPolyBG");
const ta8 = document.createElement("TABLE");ta8.setAttribute("id", "TBsphere");
const tr8 = document.createElement("TR");tr8.setAttribute("style", "border:1px");
const td25 = document.createElement("TD");td25.setAttribute("style", "width: 10%;");const td26 = document.createElement("TD");td26.setAttribute("style", "width: 80%;");const td27 = document.createElement("TD");td27.setAttribute("style", "width: 100%;");
const tn10 = document.createTextNode("ImgX:");
 i8 = document.createElement("INPUT");i8.setAttribute("min", "0");i8.setAttribute("value", "0");i8.setAttribute("max", "255");i8.setAttribute("type", "range");i8.setAttribute("class", "slider");i8.setAttribute("id", "PolyBGopacity");
const bt10 = document.createElement("BUTTON");bt10.setAttribute("id", "opacityoutPolyBG");
const ta9 = document.createElement("TABLE");ta9.setAttribute("id", "TBsphere");
const tr9 = document.createElement("TR");tr9.setAttribute("style", "border:1px");
const td28 = document.createElement("TD");td28.setAttribute("style", "width: 10%;");const td29 = document.createElement("TD");td29.setAttribute("style", "width: 80%;");const td30 = document.createElement("TD");td30.setAttribute("style", "width: 100%;");
const tn11 = document.createTextNode("ImgY:");
 i9 = document.createElement("INPUT");i9.setAttribute("min", "0");i9.setAttribute("value", "0");i9.setAttribute("max", "255");i9.setAttribute("type", "range");i9.setAttribute("class", "slider");i9.setAttribute("id", "PolyBGopacity");
const bt11 = document.createElement("BUTTON");//bt11.setAttribute("id", "opacityoutPolyBG");
document.getElementById("frame").append(dp0);dp0.append(dph);dp0.append(dim0);dim0.appendChild(thc);
dim0.append(ta0);ta0.append(tr0);
dim0.append(ta1);ta1.append(tr1);
dim0.append(ta2);ta2.append(tr2);
dim0.append(ta3);ta3.append(tr3);
dim0.append(ta4);ta4.append(tr4);
dim0.append(ta5);ta5.append(tr5);
dim0.append(ta6);ta6.append(tr6);
dim0.append(ta7);ta7.append(tr7);
dim0.append(ta8);ta8.append(tr8);
dim0.append(ta9);ta9.append(tr9);
tr0.append(td0);td0.append(bt0);bt0.append(tn0);
tr0.append(td1);td0.append(bt1);bt1.append(tn1);
tr0.append(td2);td2.append(tn2);
tr0.append(td3);td3.append(bt2);
tr0.append(td4);td4.append(tn3);
tr0.append(td5);td5.append(bt3);
tr1.append(td6);td6.append(di0);di0.append(i0);td6.append(di1);di1.append(i1);td6.append(di2);di2.append(sl0);td6.appendChild(sg0);sg0.appendChild(g);g.appendChild(po);g.append(def);def.append(pat);pat.append(img);
tr2.append(td7);tr2.append(td8);tr2.append(td9);td7.append(tn4);td8.append(i2);td9.append(bt4);
tr3.append(td10);tr3.append(td11);tr3.append(td12);td10.append(tn5);td11.append(i3);td12.append(bt5);
tr4.append(td13);tr4.append(td14);tr4.append(td15);td13.append(tn6);td14.append(i4);td15.append(bt6);
tr5.append(td16);tr5.append(td17);tr5.append(td18);td16.append(tn7);td17.append(i5);td18.append(bt7);
tr6.append(td19);tr6.append(td20);tr6.append(td21);//td19.append(tn8);
di2.append(btimg0);btimg0.append(tnimg0);di2.append(btcol0);btcol0.append(tnicol0);//td20.append(i6);td21.append(bt8);
//tr7.append(td22);tr7.append(td23);tr7.append(td24);td22.append(tn9);td23.append(i7);td24.append(bt9);
//tr8.append(td25);tr8.append(td26);tr8.append(td27);td25.append(tn10);td26.append(i8);td27.append(bt10);
//tr9.append(td28);tr9.append(td29);tr9.append(td30);td28.append(tn11);td29.append(i9);td30.append(bt11);
dragElement(dp0);
bt2.innerHTML = i0.value;i0.oninput = function() {bt2.innerHTML = this.value;verticX = bt2.innerHTML;}
bt3.innerHTML = i1.value;i1.oninput = function() {bt3.innerHTML = this.value;verticY = bt3.innerHTML;}
bt4.innerHTML = i2.value;i2.oninput = function() {bt4.innerHTML = this.value;PolyBGBred = bt4.innerHTML;}
bt5.innerHTML = i3.value;i3.oninput = function() {bt5.innerHTML = this.value;PolyBGBgreen = bt5.innerHTML;}
bt6.innerHTML = i4.value;i4.oninput = function() {bt6.innerHTML = this.value;PolyBGBblue = bt6.innerHTML;}
bt7.innerHTML = i5.value;i5.oninput = function() {bt7.innerHTML = this.value;PolyBGBopacity = bt7.innerHTML/100;}
bt8.innerHTML = i6.value;i6.oninput = function() {bt8.innerHTML = this.value;faceH = bt8.innerHTML;}
bt9.innerHTML = i7.value;i7.oninput = function() {bt9.innerHTML = this.value;faceW = bt9.innerHTML;}
bt10.innerHTML = i8.value;i8.oninput = function() {bt10.innerHTML = this.value;i8.value = bt10.innerHTML;}
bt11.innerHTML = i9.value;i9.oninput = function() {bt11.innerHTML = this.value;i9.value = bt11.innerHTML;}
bt2.onclick = function() { changeVX(); };function changeVX() { var txt; var person = prompt("Enter X coordenate:"); if (person == null || person == "") { txt = "User cancelled the prompt."; } else { txt =  person; } verticX = txt;bt2.innerHTML = txt;i0.value = txt;}
bt3.onclick = function() { changeVY(); };function changeVY() { var txt; var person = prompt("Enter Y coordenate:"); if (person == null || person == "") { txt = "User cancelled the prompt.";  } else { txt =  person; } verticY = txt;bt3.innerHTML = txt;i1.value = txt;}
bt4.onclick = function() { changeCR(); };function changeCR() { var txt; var person = prompt("Enter Y coordenate:"); if (person == null || person == "") { txt = "User cancelled the prompt.";  } else { txt =  person; } PolyBGBred = txt;bt4.innerHTML = txt;i2.value = txt;}
bt5.onclick = function() { changeCB(); };function changeCB() { var txt; var person = prompt("Enter Y coordenate:"); if (person == null || person == "") { txt = "User cancelled the prompt.";  } else { txt =  person; } PolyBGBgreen = txt;bt5.innerHTML = txt;i3.value = txt;}
bt6.onclick = function() { changeVG(); };function changeVG() { var txt; var person = prompt("Enter Y coordenate:"); if (person == null || person == "") { txt = "User cancelled the prompt.";  } else { txt =  person; } PolyBGBblue = txt;bt6.innerHTML = txt;i4.value = txt;}
bt7.onclick = function() { changeCO(); };function changeCO() { var txt; var person = prompt("Enter Y coordenate:"); if (person == null || person == "") { txt = "User cancelled the prompt.";  } else { txt =  person; } PolyBGBopacity = txt;bt7.innerHTML = txt;i5.value = txt;}
bt8.onclick = function() { changeIH(); };function changeIH() { var txt; var person = prompt("Enter Y coordenate:"); if (person == null || person == "") { txt = "User cancelled the prompt.";  } else { txt =  person; } faceW = txt;bt8.innerHTML = txt;i5.value = txt;}
bt9.onclick = function() { changeIW(); };function changeIW() { var txt; var person = prompt("Enter Y coordenate:"); if (person == null || person == "") { txt = "User cancelled the prompt.";  } else { txt =  person; } faceW = txt;bt9.innerHTML = txt;i5.value = txt;}
bt10.onclick = function() { changeCO(); };function changeCO() { var txt; var person = prompt("Enter Y coordenate:"); if (person == null || person == "") { txt = "User cancelled the prompt.";  } else { txt =  person; } i8.value = txt;bt10.innerHTML = txt;i5.value = txt;}
bt11.onclick = function() { changeCO(); };function changeCO() { var txt; var person = prompt("Enter Y coordenate:"); if (person == null || person == "") { txt = "User cancelled the prompt.";  } else { txt =  person; } i9.value = txt;bt11.innerHTML = txt;i5.value = txt;}
return i6,i7,i8,i9,thc,sg0,g,po,pat,img;}
function checkvertL() {var i = 0, select = document.getElementById('vector');for (var prop in vector) {	var t = +i++;var option = document.createElement('option');option.innerHTML = "v"+t;option.value = t;select.append(option)}};
function VListupdate(){	var select = document.getElementById("vector");select.options.length = 0;checkvertL();};
function selectOptionV(){var select = document.getElementById("vector"),i = select.selectedIndex;vertice = select.options[i].value;return vertice;};
function createPolyobj() {
	var genID = "",
	elmnt = document.getElementById(mouseS);
	for (var i = 0; i < 5; i++)	genID += possible.charAt(Math.floor(Math.random() * possible.length));
		shapeP = document.createElementNS(svgNS,"polygon");
	shapeP.setAttributeNS(null,"id","Poly_"+genID+"");
	shapePID = shapeP.id;elmnt.appendChild(shapeP);
};

function addVertice(){vector.push("0,0");VListupdate();}
function rmVertice(){delete vector[vertice];VListupdate();}
var vectorArrang = function() {var vectorformula = "";var p;for (p in vector) {vectorformula += vector[p] + " ";}return vectorformula;}
function Floatupdatepoly() {var vectorformula = "";var p;for (p in vector) {vectorformula += vector[p] + " ";}}
function polyprevDH() {if (sg0.style.display === "none") {sg0.style.display = "block";} else {sg0.style.display = "none";}};
function creatList() {
thc.onmousedown = function(e) {
function getPosition(el) {var x=0,y= 0;while(el) {x+=(el.offsetLeft-el.scrollLeft+el.clientLeft);y+=(el.offsetTop-el.scrollTop+el.clientTop);el=el.offsetParent;}return{x:x,y:y}}
function ThzhotspotPosition(evt, el, hotspotsize) {var left = el.offsetLeft;var top = el.offsetTop,hotspot = hotspotsize ? hotspotsize : 0; xv = (evt.pageX - left - (hotspot / 2));yv = (evt.pageY - top - (hotspot / 2)); return {xv: xv,yv: yv};}var a = 1;var hp = ThzhotspotPosition(e, this, 0),tempdiv = ((1<<24)*Math.random()|0).toString(16);
var hotspot = document.createElementNS(svgNS, "circle");hotspot.setAttributeNS(null,"cx",hp.xv-335);hotspot.setAttributeNS(null,"cy",hp.yv-150);hotspot.setAttribute("r",5);hotspot.setAttributeNS(null,"width",5);hotspot.setAttributeNS(null,"height",5);hotspot.setAttributeNS(null,"fill", "blue");hotspot.setAttribute("id", tempdiv);hotspot.setAttribute("class", "polyDOT");verticX = hp.xv -335;verticY = hp.yv -150;
if (polymask == 1) {i6.value;i7.value;} else {}
var interval = setInterval(function() {
var vectorformula = "",p; for (p in vector) {vectorformula += vector[p] + " ";}shapeP.setAttributeNS(null, "points", ""+vectorformula+"");po.setAttributeNS(null, "points", ""+vectorformula+"");vector[vertice] = ""+verticX+","+verticY+"";
img.setAttributeNS(null,"width",""+i6.value+"");
img.setAttributeNS(null,"height",""+i7.value+"");
img.setAttributeNS(null,"x",""+i8.value+"");
img.setAttributeNS(null,"y",""+i9.value+"");
}, refresh);
sg0.appendChild(hotspot);};};
//Copyright (C) 2018 Manuel Janz Pereira
//Autor: Manuel Janz Pereira necasjanz@gmail.com