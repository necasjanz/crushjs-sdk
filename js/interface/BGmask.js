function BGmask() {
const dc0 = document.createElement("div");dc0.setAttribute("id", "BGmaskWin");dc0.setAttribute("ondragstart", "dragStart(event)");dc0.setAttribute("draggable", "draggable='true'");dc0.style.position = 'absolute',dc0.style.width = '270px',dc0.style.height = '350px',dc0.style.left = '360px',dc0.style.top = '140px',dc0.style.zIndex = '90',dc0.style.backgroundColor = uiBK,dc0.style.textAlign = 'center',dc0.style.border = '1px solid #d3d3d3',dc0.style.display = 'none';
const dch = document.createElement("div");dch.setAttribute("id", "BGmaskWinheader");dch.style.padding = '10px',dch.style.cursor = 'move',dch.style.zIndex = '100',dch.style.backgroundColor = uiHD,dch.style.color = '#fff';
const dim0 = document.createElement("div");dim0.setAttribute("id", "toolsScale");dim0.style.color = 'white',dim0.style.width = '100%',dim0.style.height = '100%',dim0.style.left = '0%',dim0.style.top = '20%',dim0.style.overflow = 'overflow',dim0.style.position = 'absolute';
const ta0 = document.createElement("TABLE");ta0.setAttribute("id", "TBsphere");
const tr0 = document.createElement("TR");tr0.setAttribute("style", "border:1px");
const td0 = document.createElement("TD");const td1 = document.createElement("TD");const td2 = document.createElement("TD");const td3 = document.createElement("TD");
const i0 = document.createElement("INPUT");i0.setAttribute("type", "range");i0.setAttribute("min", "0");i0.setAttribute("value", "0");i0.setAttribute("max", "255");i0.setAttribute("class", "slider");
const tn0 = document.createTextNode("Center X:");
const bt0 = document.createElement("BUTTON");
const tr1 = document.createElement("TR");tr1.setAttribute("style", "border:1px");
const td4 = document.createElement("TD");const td5= document.createElement("TD");const td6 = document.createElement("TD");
const i1 = document.createElement("INPUT");i1.setAttribute("min", "0");i1.setAttribute("value", "0");i1.setAttribute("max", "255");i1.setAttribute("type", "range");i1.setAttribute("class", "slider");
const tn1 = document.createTextNode("Center Y:");
const bt1 = document.createElement("BUTTON");  
const tr2 = document.createElement("TR");tr2.setAttribute("style", "border:1px");
const td7 = document.createElement("TD");const td8 = document.createElement("TD");const td9 = document.createElement("TD");
const i2 = document.createElement("INPUT");i2.setAttribute("min", "0");i2.setAttribute("value", "255");i2.setAttribute("max", "255");i2.setAttribute("type", "range");i2.setAttribute("class", "slider");
const tn2 = document.createTextNode("Red:");
const bt2 = document.createElement("BUTTON");
const tr3 = document.createElement("TR");tr3.setAttribute("style", "border:1px");
const td10 = document.createElement("TD");const td11 = document.createElement("TD");const td12 = document.createElement("TD");
const i3 = document.createElement("INPUT");i3.setAttribute("min", "0");i3.setAttribute("value", "255");i3.setAttribute("max", "255");i3.setAttribute("type", "range");i3.setAttribute("class", "slider");
const tn3 = document.createTextNode("Green:");
const bt3 = document.createElement("BUTTON"); 
const tr4 = document.createElement("TR");tr4.setAttribute("style", "border:1px");
const td13 = document.createElement("TD");const td14 = document.createElement("TD");const td15 = document.createElement("TD");
const i4 = document.createElement("INPUT");i4.setAttribute("min", "0");i4.setAttribute("value", "255");i4.setAttribute("max", "255");i4.setAttribute("type", "range");i4.setAttribute("class", "slider");
const tn4 = document.createTextNode("Blue:");
const bt4 = document.createElement("BUTTON"); 
const tr5 = document.createElement("TR");tr5.setAttribute("style", "border:1px");
const td16 = document.createElement("TD");const td17 = document.createElement("TD");const td18 = document.createElement("TD");
const i5 = document.createElement("INPUT");i5.setAttribute("min", "0");i5.setAttribute("value", "100");i5.setAttribute("max", "100");i5.setAttribute("type", "range");i5.setAttribute("class", "slider");
const tn5 = document.createTextNode("Opacity:");
const bt5 = document.createElement("BUTTON"); 
const tr6 = document.createElement("TR");tr6.setAttribute("style", "border:1px");
const td19 = document.createElement("TD");const td20 = document.createElement("TD");const td21 = document.createElement("TD");
const i6 = document.createElement("INPUT");i6.setAttribute("min", "0");i6.setAttribute("value", "0");i6.setAttribute("max", "255");i6.setAttribute("type", "range");i6.setAttribute("class", "slider");
const tn6 = document.createTextNode("Red:");
const bt6 = document.createElement("BUTTON");
const tr7 = document.createElement("TR");tr7.setAttribute("style", "border:1px");
const td22 = document.createElement("TD");const td23 = document.createElement("TD");const td24 = document.createElement("TD");
const i7 = document.createElement("INPUT");i7.setAttribute("min", "0");i7.setAttribute("value", "0");i7.setAttribute("max", "255");i7.setAttribute("type", "range");i7.setAttribute("class", "slider");
const tn7 = document.createTextNode("Green:");
const bt7 = document.createElement("BUTTON"); 
const tr8 = document.createElement("TR");tr8.setAttribute("style", "border:1px");
const td25 = document.createElement("TD");const td26 = document.createElement("TD");const td27 = document.createElement("TD");
const i8 = document.createElement("INPUT");i8.setAttribute("min", "0");i8.setAttribute("value", "0");i8.setAttribute("max", "255");i8.setAttribute("type", "range");i8.setAttribute("class", "slider");
const tn8 = document.createTextNode("Blue:");
const bt8 = document.createElement("BUTTON"); 
const tr9 = document.createElement("TR");tr9.setAttribute("style", "border:1px");
const td28 = document.createElement("TD");const td29 = document.createElement("TD");const td30 = document.createElement("TD");
const i9 = document.createElement("INPUT");i9.setAttribute("min", "0");i9.setAttribute("value", "100");i9.setAttribute("max", "100");i9.setAttribute("type", "range");i9.setAttribute("class", "slider");
const tn9 = document.createTextNode("Opacity:");
const bt9 = document.createElement("BUTTON"); 
const tr10 = document.createElement("TR");tr10.setAttribute("style", "border:1px");
const td31 = document.createElement("TD");const td32 = document.createElement("TD");const td33 = document.createElement("TD");
const i10 = document.createElement("INPUT");i10.setAttribute("min", "0");i10.setAttribute("value", "255");i10.setAttribute("max", "255");i10.setAttribute("type", "range");i10.setAttribute("class", "slider");
const tn10 = document.createTextNode("Fill:");
const bt10 = document.createElement("BUTTON"); 
document.getElementById("frame").append(dc0);dc0.append(dch);dc0.append(dim0);dim0.appendChild(ta0);
ta0.appendChild(tr0);ta0.appendChild(tr1);ta0.appendChild(tr2);ta0.appendChild(tr3);ta0.appendChild(tr4);ta0.appendChild(tr5);
ta0.appendChild(tr6);ta0.appendChild(tr7);ta0.appendChild(tr8);ta0.appendChild(tr9);ta0.appendChild(tr10);
tr0.appendChild(td1);tr0.appendChild(td2);tr0.appendChild(td3);td1.appendChild(i0);td2.appendChild(bt0);td3.appendChild(tn0);
tr1.appendChild(td4);tr1.appendChild(td5);tr1.appendChild(td6);td4.appendChild(i1);td5.appendChild(bt1);td6.appendChild(tn1);
tr2.appendChild(td7);tr2.appendChild(td8);tr2.appendChild(td9);td7.appendChild(i2);td8.appendChild(bt2);td9.appendChild(tn2);
tr3.appendChild(td10);tr3.appendChild(td11);tr3.appendChild(td12);td10.appendChild(i3);td11.appendChild(bt3);td12.appendChild(tn3);
tr4.appendChild(td13);tr4.appendChild(td14);tr4.appendChild(td15);td13.appendChild(i4);td14.appendChild(bt4);td15.appendChild(tn4);
tr5.appendChild(td16);tr5.appendChild(td17);tr5.appendChild(td18);td16.appendChild(i5);td17.appendChild(bt5);td18.appendChild(tn5);
tr6.appendChild(td19);tr6.appendChild(td20);tr6.appendChild(td21);td19.appendChild(i6);td20.appendChild(bt6);td21.appendChild(tn6);
tr7.appendChild(td22);tr7.appendChild(td23);tr7.appendChild(td24);td22.appendChild(i7);td23.appendChild(bt7);td24.appendChild(tn7);
tr8.appendChild(td25);tr8.appendChild(td26);tr8.appendChild(td27);td25.appendChild(i8);td26.appendChild(bt8);td27.appendChild(tn8);
tr9.appendChild(td28);tr9.appendChild(td29);tr9.appendChild(td30);td28.appendChild(i9);td29.appendChild(bt9);td30.appendChild(tn9);
tr10.appendChild(td31);tr10.appendChild(td32);tr10.appendChild(td33);td31.appendChild(i10);td32.appendChild(bt10);td33.appendChild(tn10);
bt0.innerHTML = i0.value;i0.oninput = function() {  bt0.innerHTML = this.value;SgradX = bt0.innerHTML;}
bt1.innerHTML = i1.value;i1.oninput = function() {  bt1.innerHTML = this.value;SgradY = bt1.innerHTML;}
bt2.innerHTML = i2.value;i2.oninput = function() {  bt2.innerHTML = this.value;SG1red = bt2.innerHTML;}
bt3.innerHTML = i3.value;i3.oninput = function() {  bt3.innerHTML = this.value;SG1green = bt3.innerHTML;}
bt4.innerHTML = i4.value;i4.oninput = function() {  bt4.innerHTML = this.value;SG1blue = bt4.innerHTML;}
bt5.innerHTML = i5.value;i5.oninput = function() {  bt5.innerHTML = this.value;SG1opacity = bt5.innerHTML/100;}
bt6.innerHTML = i6.value;i6.oninput = function() {  bt6.innerHTML = this.value;SG2red = bt6.innerHTML;}
bt7.innerHTML = i7.value;i7.oninput = function() {  bt7.innerHTML = this.value;SG2green = bt7.innerHTML;}
bt8.innerHTML = i8.value;i8.oninput = function() {  bt8.innerHTML = this.value;SG2blue = bt8.innerHTML;}
bt9.innerHTML = i9.value;i9.oninput = function() {  bt9.innerHTML = this.value;SG3opacity = bt9.innerHTML/100;}
bt10.innerHTML = i10.value;i10.oninput = function() {  bt10.innerHTML = this.value;maskfill = bt10.innerHTML;}
bt0.onclick = function() {  var txt; var person = prompt("Enter X scale:"); if (person == null || person == "") { txt = "User cancelled the prompt."; } else { txt =  person; } SgradX = txt;bt0.innerHTML = txt;i0.value = txt;}
bt1.onclick = function() {  var txt; var person = prompt("Enter Y scale:"); if (person == null || person == "") { txt = "User cancelled the prompt.";  } else { txt =  person; } SgradY = txt;bt1.innerHTML = txt;i1.value = txt;}
bt2.onclick = function() {  var txt; var person = prompt("Enter Z scale:"); if (person == null || person == "") { txt = "User cancelled the prompt."; } else { txt =  person; } SG1red = txt;bt2.innerHTML = txt;i2.value = txt;}
bt3.onclick = function() {  var txt; var person = prompt("Enter Z scale:"); if (person == null || person == "") { txt = "User cancelled the prompt."; } else { txt =  person; } SG1green = txt;bt3.innerHTML = txt;i3.value = txt;}
bt4.onclick = function() {  var txt; var person = prompt("Enter Z scale:"); if (person == null || person == "") { txt = "User cancelled the prompt."; } else { txt =  person; } SG1blue = txt;bt4.innerHTML = txt;i4.value = txt;}
bt5.onclick = function() {  var txt; var person = prompt("Enter Z scale:"); if (person == null || person == "") { txt = "User cancelled the prompt."; } else { txt =  person; } SG1opacity = txt;bt5.innerHTML = txt;i5.value = txt;}
bt6.onclick = function() {  var txt; var person = prompt("Enter Z scale:"); if (person == null || person == "") { txt = "User cancelled the prompt."; } else { txt =  person; } SG2red = txt;bt6.innerHTML = txt;i6.value = txt;}
bt7.onclick = function() {  var txt; var person = prompt("Enter Z scale:"); if (person == null || person == "") { txt = "User cancelled the prompt."; } else { txt =  person; } SG2green = txt;bt7.innerHTML = txt;i7.value = txt;}
bt8.onclick = function() {  var txt; var person = prompt("Enter Z scale:"); if (person == null || person == "") { txt = "User cancelled the prompt."; } else { txt =  person; } SG2blue = txt;bt8.innerHTML = txt;i8.value = txt;}
bt9.onclick = function() {  var txt; var person = prompt("Enter Z scale:"); if (person == null || person == "") { txt = "User cancelled the prompt."; } else { txt =  person; } SG3opacity = txt;bt9.innerHTML = txt;i9.value = txt;}
bt10.onclick = function() {  var txt; var person = prompt("Enter Z scale:"); if (person == null || person == "") { txt = "User cancelled the prompt."; } else { txt =  person; } maskfill = txt;bt10.innerHTML = txt;i10.value = txt;}
dragElement(document.getElementById(("BGmaskWin")));
}
//Copyright (C) 2018 Manuel Janz Pereira
//Autor: Manuel Janz Pereira necasjanz@gmail.com