function skew() {
const dk0 = document.createElement("div");dk0.setAttribute("id", "skewWin");dk0.setAttribute("ondragstart", "dragStart(event)");dk0.setAttribute("draggable", "draggable='true'");
const dkh = document.createElement("div");dkh.setAttribute("id", "skewWinheader");
const dim0 = document.createElement("div");dim0.setAttribute("id", "toolsSkew");dim0.style.color = 'white',dim0.style.width = '100%',dim0.style.height = '100%',dim0.style.left = '0%',dim0.style.top = '20%',dim0.style.overflow = 'overflow',dim0.style.position = 'absolute';
const ta0 = document.createElement("TABLE");ta0.setAttribute("id", "TBsphere");const tr0 = document.createElement("TR");tr0.setAttribute("style", "border:1px");const td0 = document.createElement("TD");const td1 = document.createElement("TD");const td2 = document.createElement("TD");
const i0 = document.createElement("INPUT");i0.setAttribute("type", "range");i0.setAttribute("min", "-2000");i0.setAttribute("value", "0");i0.setAttribute("max", "2000");i0.setAttribute("id", "faceSX");i0.setAttribute("class", "slider");
const ch0 = document.createElement("INPUT");ch0.setAttribute("type", "checkbox");
const bt0 = document.createElement("BUTTON");
const tr1 = document.createElement("TR");tr1.setAttribute("style", "border:1px");const td3 = document.createElement("TD");const td4 = document.createElement("TD");
const i1 = document.createElement("INPUT");i1.setAttribute("min", "-2000");i1.setAttribute("value", "0");i1.setAttribute("max", "2000");i1.setAttribute("type", "range");i1.setAttribute("id", "faceSY");i1.setAttribute("class", "slider");
const td5 = document.createElement("TD");const tr2 = document.createElement("TR");tr2.setAttribute("style", "border:1px");
const ch1 = document.createElement("INPUT");ch1.setAttribute("type", "checkbox");
const bt1 = document.createElement("BUTTON"); 
dk0.append(dkh);document.getElementById("frame").append(dk0);dk0.append(dim0);document.getElementById("frame").append(dk0);dim0.appendChild(ta0);
ta0.appendChild(tr0);ta0.appendChild(tr1);ta0.appendChild(tr2);tr0.appendChild(td0);tr0.appendChild(td1);tr0.appendChild(td2);td0.appendChild(i0);td1.appendChild(ch0);td2.appendChild(bt0);
tr1.appendChild(td3);tr1.appendChild(td4);tr1.appendChild(td5);td3.appendChild(i1);td4.appendChild(ch1);td5.appendChild(bt1);
bt0.innerHTML = i0.value;i0.oninput = function() {  bt0.innerHTML = this.value;faceSkewX = bt0.innerHTML/10;}
bt1.innerHTML = i1.value;i1.oninput = function() {  bt1.innerHTML = this.value;faceSkewY = bt1.innerHTML/10;}
bt0.onclick = function() { changeSkx(); };function changeSkx() { var txt; var person = prompt("Enter X skew:"); if (person == null || person == "") { txt = "User cancelled the prompt."; } else { txt =  person; } actSKX = 1;faceSkewX = txt/10;bt0.innerHTML = txt;i0.value = txt;}//Change sX
bt1.onclick = function() { changeSky(); };function changeSky() { var txt; var person = prompt("Enter Y skew:"); if (person == null || person == "") { txt = "User cancelled the prompt.";  } else { txt =  person; } actSKY = 1;faceSkewY = txt/10;bt1.innerHTML = txt;i1.value = txt;}//Change sY
dragElement(document.getElementById(("skewWin")));
}
//Copyright (C) 2018 Manuel Janz Pereira
//Autor: Manuel Janz Pereira necasjanz@gmail.com