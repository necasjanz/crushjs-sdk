function translate() {
const dt0 = document.createElement("div");dt0.setAttribute("id", "translateWin");dt0.setAttribute("ondragstart", "dragStart(event)");dt0.setAttribute("draggable", "draggable='true'");dt0.style.position = 'absolute',dt0.style.width = '210px',dt0.style.height = '100px',dt0.style.left = '300px',dt0.style.top = '100px',dt0.style.zIndex = '90',dt0.style.backgroundColor = uiBK,dt0.style.textAlign = 'center',dt0.style.border = '1px solid #d3d3d3',dt0.style.display = 'none';
const dth = document.createElement("div");dth.setAttribute("id", "translateWinheader");dth.style.padding = '10px',dth.style.cursor = 'move',dth.style.zIndex = '100',dth.style.backgroundColor =uiHD,dth.style.color = '#fff';
const dim0 = document.createElement("div");dim0.setAttribute("id", "toolsTranslate");dim0.style.color = 'white',dim0.style.width = '100%',dim0.style.height = '100%',dim0.style.left = '0%',dim0.style.top = '20%',dim0.style.overflow = 'overflow',dim0.style.position = 'absolute';
const ta0 = document.createElement("TABLE");ta0.setAttribute("id", "TBsphere");const tr0 = document.createElement("TR");tr0.setAttribute("style", "border:1px");const td0 = document.createElement("TD");const td1 = document.createElement("TD");const td2 = document.createElement("TD");
const i0 = document.createElement("INPUT");i0.setAttribute("type", "range");i0.setAttribute("min", "-2000");i0.setAttribute("value", "0");i0.setAttribute("max", "2000");i0.setAttribute("id", "faceX");i0.setAttribute("class", "slider");i0.style.width='100%',i0.style.height='5px',i0.style.borderRadius='2.5px',i0.style.background=Xcolor,i0.style.outline='none';
const ch0 = document.createElement("INPUT");ch0.setAttribute("type", "checkbox");ch0.onclick = function() { if (ch0.checked == true){actX3d = 1;} else {actX3d = 0;} };
const bt0 = document.createElement("BUTTON");bt0.onclick = function() { changePx();};
const tr1 = document.createElement("TR");tr1.setAttribute("style", "border:1px");const td3 = document.createElement("TD");const td4 = document.createElement("TD");const td5 = document.createElement("TD");
const i1 = document.createElement("INPUT");i1.setAttribute("min", "-2000");i1.setAttribute("value", "0");i1.setAttribute("max", "2000");i1.setAttribute("type", "range");i1.setAttribute("id", "faceY");i1.setAttribute("class", "slider");i1.style.width='100%',i1.style.height='5px',i1.style.borderRadius='2.5px',i1.style.background=Ycolor,i1.style.outline='none';
const ch1 = document.createElement("INPUT");ch1.setAttribute("type", "checkbox");ch1.onclick = function() { if (ch1.checked == true){actY3d = 1;} else {actY3d = 0;} };
const bt1 = document.createElement("BUTTON");bt1.onclick = function() { changePy();};
const tr2 = document.createElement("TR");tr2.setAttribute("style", "border:1px");const td6 = document.createElement("TD");const td7 = document.createElement("TD");const td8 = document.createElement("TD");
const i2 = document.createElement("INPUT");i2.setAttribute("min", "-2000");i2.setAttribute("value", "0");i2.setAttribute("max", "2000");i2.setAttribute("type", "range");i2.setAttribute("id", "faceZ");i2.setAttribute("class", "slider");i2.style.width='100%',i2.style.height='5px',i2.style.borderRadius='2.5px',i2.style.background=Zcolor,i2.style.outline='none';
const ch2 = document.createElement("INPUT");ch2.setAttribute("type", "checkbox");ch2.onclick = function() { if (ch2.checked == true){actZ3d = 1;} else {actZ3d = 0;} };
const bt2 = document.createElement("BUTTON");bt2.onclick = function() { changePz();};
document.getElementById("frame").append(dt0);dt0.append(dth);dt0.append(dim0);document.getElementById("frame").append(dt0);dim0.appendChild(ta0);
ta0.appendChild(tr0);ta0.appendChild(tr1);ta0.appendChild(tr2);tr0.appendChild(td0);tr0.appendChild(td1);tr0.appendChild(td2);td0.appendChild(i0);td1.appendChild(ch0);td2.appendChild(bt0);
tr1.appendChild(td3);tr1.appendChild(td4);tr1.appendChild(td5);td3.appendChild(i1);td4.appendChild(ch1);td5.appendChild(bt1);tr2.appendChild(td6);tr2.appendChild(td7);tr2.appendChild(td8);
td6.appendChild(i2);td7.appendChild(ch2);td8.appendChild(bt2);
bt0.innerHTML = i0.value;i0.oninput = function() {  bt0.innerHTML = this.value;facetX = bt0.innerHTML;}
bt1.innerHTML = i1.value;i1.oninput = function() {  bt1.innerHTML = this.value;facetY = bt1.innerHTML;}
bt2.innerHTML = i2.value;i2.oninput = function() {  bt2.innerHTML = this.value;faceZ = bt2.innerHTML;}
function changePx() { var txt; var result = prompt("Enter X coordenate:"); if (result == null || result == "") { txt = "User cancelled the prompt."; } else { txt =  result; } facetX = txt; bt0.innerHTML = txt;i0.value = txt;}
function changePy() { var txt; var result = prompt("Enter Y coordenate:"); if (result == null || result == "") { txt = "User cancelled the prompt."; } else { txt =  result; } facetY = txt; bt1.innerHTML = txt;i1.value = txt;}
function changePz() { var txt; var result = prompt("Enter Z coordenate:"); if (result == null || result == "") { txt = "User cancelled the prompt."; } else { txt =  result; } faceZ = txt; bt2.innerHTML = txt;i2.value = txt;}
dragElement(document.getElementById(("translateWin")));}
//Copyright (C) 2018 Manuel Janz Pereira
//Autor: Manuel Janz Pereira necasjanz@gmail.com