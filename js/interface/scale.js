function scale() {
const ds0 = document.createElement("div");ds0.setAttribute("id", "scaleWin");ds0.setAttribute("ondragstart", "dragStart(event)");ds0.setAttribute("draggable", "draggable='true'");ds0.style.position = 'absolute',ds0.style.width = '210px',ds0.style.height = '100px',ds0.style.left = '360px',ds0.style.top = '140px',ds0.style.zIndex = '90',ds0.style.backgroundColor = uiBK,ds0.style.textAlign = 'center',ds0.style.border = '1px solid #d3d3d3',ds0.style.display = 'none';
const dsh = document.createElement("div");dsh.setAttribute("id", "scaleWinheader");dsh.style.padding = '10px',dsh.style.cursor = 'move',dsh.style.zIndex = '100',dsh.style.backgroundColor = uiHD,dsh.style.color = '#fff';
const dim0 = document.createElement("div");dim0.setAttribute("id", "toolsScale");dim0.style.color = 'white',dim0.style.width = '100%',dim0.style.height = '100%',dim0.style.left = '0%',dim0.style.top = '20%',dim0.style.overflow = 'overflow',dim0.style.position = 'absolute';
const ta0 = document.createElement("TABLE");ta0.setAttribute("id", "TBsphere");const tr0 = document.createElement("TR");tr0.setAttribute("style", "border:1px");const td0 = document.createElement("TD");const td1 = document.createElement("TD");const td2 = document.createElement("TD");
const i0 = document.createElement("INPUT");i0.setAttribute("type", "range");i0.setAttribute("min", "-100");i0.setAttribute("value", "1");i0.setAttribute("max", "100");i0.setAttribute("id", "shscaleX");i0.setAttribute("class", "slider");i0.style.width='100%',i0.style.height='5px',i0.style.borderRadius='2.5px',i0.style.background=Xcolor,i0.style.outline='none';
const ch0 = document.createElement("INPUT");ch0.setAttribute("type", "checkbox");ch0.setAttribute("id", "eSX");ch0.onclick = function() { engageSX() };
const bt0 = document.createElement("BUTTON");
const tr1 = document.createElement("TR");tr1.setAttribute("style", "border:1px");const td3 = document.createElement("TD");const td4 = document.createElement("TD");const td5 = document.createElement("TD");
const i1 = document.createElement("INPUT");i1.setAttribute("min", "-100");i1.setAttribute("value", "1");i1.setAttribute("max", "100");i1.setAttribute("type", "range");i1.setAttribute("id", "shscaleY");i1.setAttribute("class", "slider");i1.style.width='100%',i1.style.height='5px',i1.style.borderRadius='2.5px',i1.style.background=Ycolor,i1.style.outline='none';
const ch1 = document.createElement("INPUT");ch1.setAttribute("type", "checkbox");ch1.setAttribute("id", "eSY");ch1.onclick = function() { engageSY() };
const bt1 = document.createElement("BUTTON");  
const tr2 = document.createElement("TR");tr2.setAttribute("style", "border:1px");const td6 = document.createElement("TD");const td7 = document.createElement("TD");const td8 = document.createElement("TD");
const i2 = document.createElement("INPUT");i2.setAttribute("min", "-100");i2.setAttribute("value", "1");i2.setAttribute("max", "100");i2.setAttribute("type", "range");i2.setAttribute("id", "shscaleZ");i2.setAttribute("class", "slider");i2.style.width='100%',i2.style.height='5px',i2.style.borderRadius='2.5px',i2.style.background=Zcolor,i2.style.outline='none';
const ch2 = document.createElement("INPUT");ch2.setAttribute("type", "checkbox");ch2.setAttribute("id", "eSZ");ch2.onclick = function() { engageSZ() };
const bt2 = document.createElement("BUTTON"); 
document.getElementById("frame").append(ds0);ds0.append(dsh);ds0.append(dim0);dim0.appendChild(ta0);
ta0.appendChild(tr0);ta0.appendChild(tr1);ta0.appendChild(tr2);tr0.appendChild(td0);tr0.appendChild(td1);tr0.appendChild(td2);td0.appendChild(i0);td1.appendChild(ch0);td2.appendChild(bt0);
tr1.appendChild(td3);tr1.appendChild(td4);tr1.appendChild(td5);td3.appendChild(i1);td4.appendChild(ch1);td5.appendChild(bt1);tr2.appendChild(td6);tr2.appendChild(td7);tr2.appendChild(td8);
td6.appendChild(i2);td7.appendChild(ch2);td8.appendChild(bt2);
bt0.innerHTML = i0.value;i0.oninput = function() {bt0.innerHTML = this.value;Ssclx = bt0.innerHTML;}
bt1.innerHTML = i1.value;i1.oninput = function() {bt1.innerHTML = this.value;Sscly = bt1.innerHTML;}
bt2.innerHTML = i2.value;i2.oninput = function() {bt2.innerHTML = this.value;Ssclz = bt2.innerHTML;}
bt0.onclick = function() { changeSx();};function changeSx() { var txt; var person = prompt("Enter X scale:"); if (person == null || person == "") { txt = "User cancelled the prompt."; } else { txt =  person; } Ssclx = txt;var outputshpX = bt0;outputshpX.innerHTML = txt;i0.value = txt;}
bt1.onclick = function() { changeSy();};function changeSy() { var txt; var person = prompt("Enter Y scale:"); if (person == null || person == "") { txt = "User cancelled the prompt.";  } else { txt =  person; } Sscly = txt;var outputshpY = bt1;outputshpY.innerHTML = txt;i1.value = txt;}
bt2.onclick = function() { changeSz();};function changeSz() { var txt; var person = prompt("Enter Z scale:"); if (person == null || person == "") { txt = "User cancelled the prompt."; } else { txt =  person; } Ssclz = txt;var outputshpZ = bt2;outputshpZ.innerHTML = txt;i2.value = txt;}
function engageSX() {var checkBox = document.getElementById("eSX");if (checkBox.checked == true){actSX = 1;} else {actSX = 0;}}
function engageSY() {var checkBox = document.getElementById("eSY");if (checkBox.checked == true){actSY = 1;} else {actSY = 0;}}
function engageSZ() {var checkBox = document.getElementById("eSZ");if (checkBox.checked == true){actSZ = 1;} else {actSZ = 0;}}
dragElement(document.getElementById(("scaleWin")));}
//Copyright (C) 2018 Manuel Janz Pereira
//Autor: Manuel Janz Pereira necasjanz@gmail.com