function stopMouse() {  worker.terminate(); worker.postMessage({'cmd': 'stop', 'msg': 'Bye'});  }
function mouse(el){ "use strict";
var geo = document.getElementsByClassName("sphere")[0];
var bx = document.getElementById("mouseoverst");
var mx = el.pageX;
var my = el.pageY;
var boxX = bx.style.left; 
var boxY = bx.style.top;
  worker.postMessage({
    'cmd': 'mouse', 'msg': 'engage mouse',
      Rotations: {r1:rotation, r2:rotationRx, r3:rotationZ},
      MouseRx: {m1:mx, w1:my},
      MouseRy: {m2:my},
      MouseF: {env1:boxX, env2:boxY }
  }); 
document.getElementById("mouseoverst").innerHTML =mouseS;
updateFaceCounter();
bx.style;
return mx,my;
}
function dragElement(elmnt) {
  var pos1 = 0, pos2 = 0, pos3 = 0, pos4 = 0;
  if (document.getElementById(elmnt.id + "header")) {
    document.getElementById(elmnt.id + "header").onmousedown = dragMouseDown;
  } else {
    elmnt.onmousedown = dragMouseDown;
  }
  function dragMouseDown(e) {
    e = e || window.event;
    pos3 = e.clientX;
    pos4 = e.clientY;
    document.onmouseup = closeDragElement;
    document.onmousemove = elementDrag;
  }

  function elementDrag(e) {
    e = e || window.event;
    pos1 = pos3 - e.clientX;
    pos2 = pos4 - e.clientY;
    pos3 = e.clientX;
    pos4 = e.clientY;
    elmnt.style.top = (elmnt.offsetTop - pos2) + "px";
    elmnt.style.left = (elmnt.offsetLeft - pos1) + "px";
  }

  function closeDragElement() {
    document.onmouseup = null;
    document.onmousemove = null;
  }
}
function handle0() {
var el0 = document.getElementById('frame');
var el1 = document.getElementById('listDIV2');
var handle = document.getElementById('handle');

handle.addEventListener('mousedown', setup, false);

var startX = void 0,
    startWidth = void 0;

function setup(event) {
  startX = event.clientX;
  startWidth0 = parseInt(window.getComputedStyle(el0).width, 10);
startWidth1 = parseInt(window.getComputedStyle(el1).width, 10);
  document.documentElement.addEventListener('mousemove', drag, false);
  document.documentElement.addEventListener('mouseup', destroy, false);
}
function drag(event) {
  el0.style.width = startWidth0 - event.clientX + startX + 'px';
  el1.style.width = startWidth1 + event.clientX - startX + 'px';
}
function destroy(e) {
  document.documentElement.removeEventListener('mousemove', drag, false);
  document.documentElement.removeEventListener('mouseup', destroy, false);
  }
};
//fix this one
function handle1() {
var el0 = document.getElementById('frame');
var el1 = document.getElementById('listDIV0');
var handle = document.getElementById('handle1');
handle.addEventListener('mousedown', setup, false);
var startX = void 0,startWidth = void 0;
function setup(event) {
  startX = event.clientX;
  startWidth0 = parseInt(window.getComputedStyle(el0).width, 10);
startWidth1 = parseInt(window.getComputedStyle(el1).width, 10);
  document.documentElement.addEventListener('mousemove', drag, false);
  document.documentElement.addEventListener('mouseup', destroy, false);
  }
function drag(event) {
  el1.style.width = startWidth1 + -event.clientX + startX + 'px';
}
function destroy(e) {
  document.documentElement.removeEventListener('mousemove', drag, false);
  document.documentElement.removeEventListener('mouseup', destroy, false);
  }
};
function handle2() {
var el0 = document.getElementById('frame');
var el1 = document.getElementById('listDIV1');
var el2 = document.getElementById('listDIV0');
var el3 = document.getElementById('listDIV2');
var handle = document.getElementById('handle2');
handle.addEventListener('mousedown', setup, false);
var startY = void 0,startHeight = void 0;
function setup(event) {
  startY = event.clientY;
  startHeight0 = parseInt(window.getComputedStyle(el0).height, 10);
  startHeight1 = parseInt(window.getComputedStyle(el1).height, 10);
  startHeight2 = parseInt(window.getComputedStyle(el2).height, 10);
  startHeight3 = parseInt(window.getComputedStyle(el3).height, 10);
  document.documentElement.addEventListener('mousemove', drag, false);
  document.documentElement.addEventListener('mouseup', destroy, false);
}
function drag(event) {
  el0.style.height = startHeight0 + event.clientY - startY + 'px';
  el1.style.height = startHeight1 - event.clientY + startY + 'px';
  el2.style.height = startHeight2 + event.clientY - startY + 'px';
  el3.style.height = startHeight3 + event.clientY - startY + 'px';
}
function destroy(e) {
  document.documentElement.removeEventListener('mousemove', drag, false);
  document.documentElement.removeEventListener('mouseup', destroy, false);
  }
};
document.oncontextmenu = document.body.oncontextmenu = function() {return false;}      
  document.getElementById('frame').addEventListener('mousedown', function(e){
    document.getElementById('frame').addEventListener('mousemove', function(e){
 if(isControl == true && e.which == 1) {
      e.preventDefault();
  var grid, handler, pa, pb, pc, pd, resize_panel;
        grid = document.querySelector('.panel-grid');
        handler = document.getElementById('handler');
        handlerStyle = handler.style;
        pa = document.getElementById("panel-a");
        paStyle = pa.style
        pb = document.getElementById("panel-b");
        pbStyle = pb.style
        pc = document.getElementById("panel-c");
        pcStyle = pc.style
        pd = document.getElementById("panel-d");
        pdStyle = pd.style
        handlerStyle.left = e.pageX - 5 + "px";
        handlerStyle.top = e.pageY - 5 + "px";
        paStyle.width = e.pageX + "px";
        paStyle.height = e.pageY + "px";
        pbStyle.left = e.pageX + "px";
        pbStyle.height = e.pageY + "px";
        pcStyle.width = e.pageX + "px";
        pcStyle.top = e.pageY + "px";
        pdStyle.left = e.pageX + "px";
        pdStyle.top = e.pageY + "px";
   }  if (isShift == true && e.which == 2 ) {
       e.preventDefault();
       if (rotationZ == -149){
        rotationZ = MouseRyresult*2 + window.innerHeight;

      }else {
        rotationZ = -MouseRyresult*2 + window.innerHeight;
      }
      } if (isShift == true && e.which == 1 ) {
       e.preventDefault();
        rotationRx = MouseRXyresult*2;
        rotationRy = MouseRxyresult*2 ;
        divL0.style.opacity = '0.6';
        divL1.style.opacity = '0.6';
        divL2.style.opacity = '0.6';
      } if (isControl == true && e.which == 2 ) {
        XaxisX= MouseFresultX - window.innerWidth/2;
        YaxisY= MouseFresultY - window.innerHeight/2;
      } else if (isControl == false || isShift == false) {

      }
      });
      document.getElementById('frame').addEventListener('mouseup',function(){
        divL0.style.opacity = '1';
        divL1.style.opacity = '1';
        divL2.style.opacity = '1';
      });
});

function scroll(e) {
  if (e.wheelDelta) {return e.wheelDelta;}
  if (e.originalEvent.detail) {return e.originalEvent.detail * 25;}
  if (e.originalEvent && e.originalEvent.wheelDelta) {return e.originalEvent.wheelDelta;}}
    var num = 0;
    var upnum=0;
document.getElementById('frame').addEventListener('wheel', function(e) {
    var d = scroll(e);
    if(d < 0) {
sclx= --num/100;
scly= sclx;
sclz= sclx;
    } else {
sclx= ++num/100;
scly= sclx;
sclz= sclx;
    }
    e.stopPropagation();
    e.preventDefault();
    upnum=num;
    return sclx,scly,upnum;
});
function mouseOBJcontrol() {
document.oncontextmenu = document.body.oncontextmenu = function() {return false;}
  document.getElementById(selectOP).addEventListener('mousedown', function(e){
    document.getElementById('frame').addEventListener('mousemove', function(e){

      if(e.which == 1 && isM == true) {
      e.preventDefault();
        X = MouseFresultX - window.innerWidth/2;
        Y = MouseFresultY - window.innerHeight/2;
      } if (e.which == 2 && isM == true) {
       e.preventDefault();
        Z = -MouseFresultY + window.innerHeight/2;
      } else if (isControl == false || isShift == false) {
      //e.stoppropagation();
      }
      });
      document.getElementById(selectOP).addEventListener('mouseup',function(){
        document.getElementById('frame').addEventListener('mousemove', function(e){
      });
      });
});
};
//Copyright (C) 2018 Manuel Janz Pereira
//Autor: Manuel Janz Pereira necasjanz@gmail.com