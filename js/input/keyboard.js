var map={89:!1,85:!1,73:!1,79:!1,80:!1,76:!1,75:!1,72:!1,74:!1,71:!1,77:!1,78:!1,66:!1,70:!1,88:!1,90:!1,67:!1,86:!1,69:!1,82:!1,102:!1,104:!1,100:!1,98:!1,39:!1,37:!1,40:!1,38:!1,87:!1,65:!1,83:!1,68:!1,16:!1,16:!1,192:!1,73:!1,18:!1,17:!1};
var isUP = false, isDOWN = false, isLEFT = false, isRIGHT = false, isEscape = false,isControl = false,isShift = false,isQ = false,isW = false,isE = false,isR = false,isT = false,isY = false,isU = false,isI = false,isO = false,isP = false,isA = false,isS = false,isD = false,isF = false,isG = false,isH = false,isJ = false,isK = false,isL = false,isZ = false,isX = false,isC = false,isV = false,isB = false,isN = false,isM = false,isNP0 = false,isNP1 = false,isNP2 = false,isNP3 = false,isNP4 = false,isNP5 = false,isNP6 = false,isNP7 = false,isNP8 = false,isNP9 = false;
var A=65,W=87,D=68,S=83,Y = 89, U = 85, I = 73, O = 79, P = 80, L = 76, K = 75, J = 74, H = 72, G = 71, M = 77, N = 78, B = 66, V = 86, F = 70, X = 88,Z = 90,C = 67, T = 84, E = 69, R = 82,
 N6 = 102, N8 = 104, N4 = 100, N2 = 98, SKER = 39, SKEL = 37, ZOOMMIN = 40, ZOOMPLUS = 38, LEFT = 37, UP = 38,  RIGHT = 39, DOWN = 40, SPACE = 32, SHIFT = 16, DEBUG = 192, INVENTORY = 73, VAR = 18, CONTROL = 17;// WASD
window.addEventListener("keydown", keysPressed, false);
window.addEventListener("keyup", keysReleased, false);
var keys = [];
function keysPressed(e) {
keys[e.which] = true;

    if ("key" in e) { isEscape = (e.key == "Escape" || e.key == "Esc");} else { isEscape = (e.which == 27);} if (isEscape) { }
    if ("key" in e) { isControl = (e.key == "Control" || e.key == "ctrl");} else { isControl = (e.which == 17);} if (isControl) { }
    if ("key" in e) { isShift = (e.key == "Shift" || e.key == "shift");} else { isShift = (e.which == 16);} if (isShift) { }
    if ("key" in e) { isQ = (e.key == "Q" || e.key == "q");} else { isQ = (e.which === 81);} if (isQ) { }
    //if ("key" in e) { isW = (e.key == "W" || e.key == "w");} else { isW = (e.which === W);} if (isW) { isW=true;}
    if ("key" in e) { isE = (e.key == "E" || e.key == "e");} else { isE = (e.which === 69);} if (isE) { engage();}
    if ("key" in e) { isR = (e.key == "R" || e.key == "r");} else { isR = (e.which === 82);} if (isR) { remove();}
    if ("key" in e) { isT = (e.key == "T" || e.key == "t");} else { isT = (e.which === 84);} if (isT) { disengage();}
    if ("key" in e) { isY = (e.key == "Y" || e.key == "y");} else { isY = (e.which === 89);} if (isY) { }
    if ("key" in e) { isU = (e.key == "U" || e.key == "u");} else { isU = (e.which === 85);} if (isU) { }
    if ("key" in e) { isI = (e.key == "I" || e.key == "i");} else { isI = (e.which === 73);} if (isI) { }
    if ("key" in e) { isO = (e.key == "O" || e.key == "o");} else { isO = (e.which === 79);} if (isO) { }
    if ("key" in e) { isP = (e.key == "P" || e.key == "p");} else { isP = (e.which === 80);} if (isP) { }
    //if ("key" in e) { isA = (e.key == "A" || e.key == "a");} else { isA = (e.which === A);} if (isA) { isA=true;}
    //if ("key" in e) { isS = (e.key == "S" || e.key == "s");} else { isS = (e.which === S);} if (isS) { isS=true;}
    //if ("key" in e) { isD = (e.key == "D" || e.key == "d");} else { isD = (e.which === D);} if (isD) { isD=true;}
    if ("key" in e) { isF = (e.key == "F" || e.key == "f");} else { isF = (e.which === 70);} if (isF) { OBJFindH();updateAllH();}
    if ("key" in e) { isG = (e.key == "G" || e.key == "g");} else { isG = (e.which === 71);} if (isG) { }
    if ("key" in e) { isH = (e.key == "H" || e.key == "h");} else { isH = (e.which === 72);} if (isH) { }
    if ("key" in e) { isJ = (e.key == "J" || e.key == "j");} else { isJ = (e.which === 74);} if (isJ) { }
    if ("key" in e) { isK = (e.key == "K" || e.key == "k");} else { isK = (e.which === 75);} if (isK) { }
    if ("key" in e) { isL = (e.key == "L" || e.key == "l");} else { isL = (e.which === 76);} if (isL) { }
    if ("key" in e) { isZ = (e.key == "Z" || e.key == "z");} else { isZ = (e.which === 90);} if (isZ) { }
    if ("key" in e) { isX = (e.key == "X" || e.key == "x");} else { isX = (e.which === 88);} if (isX) { }
    if ("key" in e) { isC = (e.key == "C" || e.key == "c");} else { isC = (e.which === 67);} if (isC) { }
    if ("key" in e) { isV = (e.key == "V" || e.key == "v");} else { isV = (e.which === 86);} if (isV) { ativ();}
    if ("key" in e) { isB = (e.key == "B" || e.key == "b");} else { isB = (e.which === 66);} if (isB) { }
    if ("key" in e) { isN = (e.key == "N" || e.key == "n");} else { isN = (e.which === 78);} if (isN) { }
    if ("key" in e) { isM = (e.key == "M" || e.key == "m");} else { isM = (e.which === 77);} if (isM) { }
    if ("key" in e) { isNP0 = (e.key == "numpad 0" || e.key == "0");} else { isNP0 = (e.which === 92);} if (isNP0) { }
    if ("key" in e) { isNP1 = (e.key == "numpad 1" || e.key == "1");} else { isNP1 = (e.which === 97);} if (isNP1) {scxyzD();}//scale xyz+
    if ("key" in e) { isNP2 = (e.key == "numpad 2" || e.key == "2");} else { isNP2 = (e.which === 98);} if (isNP2) {scyD();}
    if ("key" in e) { isNP3 = (e.key == "numpad 3" || e.key == "3");} else { isNP3 = (e.which === 99);} if (isNP3) {scxD();}//scale zy-
    if ("key" in e) { isNP4 = (e.key == "numpad 4" || e.key == "4");} else { isNP4 = (e.which === 100);} if (isNP4) {ct1();}
    if ("key" in e) { isNP5 = (e.key == "numpad 5" || e.key == "5");} else { isNP5 = (e.which === 101);} if (isNP5) { }
    if ("key" in e) { isNP6 = (e.key == "numpad 6" || e.key == "6");} else { isNP6 = (e.which === 102);} if (isNP6) {ct2();}
    if ("key" in e) { isNP7 = (e.key == "numpad 7" || e.key == "7");} else { isNP7 = (e.which === 103);} if (isNP7) {scxyzA();}//scale xyz-
    if ("key" in e) { isNP8 = (e.key == "numpad 8" || e.key == "8");} else { isNP8 = (e.which === 104);} if (isNP8) {scyA();}
    if ("key" in e) { isNP9 = (e.key == "numpad 9" || e.key == "9");} else { isNP9 = (e.which === 105);} if (isNP9) {scxA();}//scale zy-
      /*  switch(e.which) {
        case 37:rotation++;break;
        case 38:rotationRx++;break;
        case 39:rotation--;break;
        case 40:rotationRx--;break;  
        case 65:isA=true;break;
        case 87:isW=true;break;
        case 68:isD=true;break;
        case 83:isS=true;break;  
        e.preventDefault(); 
    }  */

      if (e.which === W) { isW = true;} 
      if (e.which === A) { isA = true;} 
      if (e.which === S) { isS = true;} 
      if (e.which === D) { isD = true;}

      if (e.which === UP) { isUP = true;rotationRx++;} 
      if (e.which === DOWN) { isDOWN = true;rotationRx--;} 
      if (e.which === LEFT) { isLEFT = true;rotation--;} 
      if (e.which === RIGHT) { isRIGHT = true;rotation++;}
};
function keysReleased(e) {
//keys[e.keyCode] = false;
  if (e.keyCode in map) {
        map[e.keyCode] = true;
               if (map[SHIFT] && map) {
           isShift = true;
        }
      if (e.which === W) { isW = false; }
      if (e.which === S) { isA = false; } 
      if (e.which === S) { isS = false; } 
      if (e.which === D) { isD = false; }
      if (e.which === UP) { isUP = false;} 
      if (e.which === DOWN) { isDOWN = false;} 
      if (e.which === LEFT) { isLEFT = false;} 
      if (e.which === RIGHT) { isRIGHT = false;}
    isUP = false, isDOWN = false, isLEFT = false, isRIGHT = false, isEscape = false,isControl = false,isShift = false,isQ = false,isW = false,isE = false,isR = false,isT = false,isY = false,isU = false,isI = false,isO = false,isP = false,isA = false,isS = false,isD = false,isF = false,isG = false,isH = false,isJ = false,isK = false,isL = false,isZ = false,isX = false,isC = false,isV = false,isB = false,isN = false,isM = false,isNP0 = false,isNP1 = false,isNP2 = false,isNP3 = false,isNP4 = false,isNP5 = false,isNP6 = false,isNP7 = false,isNP8 = false,isNP9 = false;
    }
};
//if (isW){y=Ysub;}if (isD){x=Xadd;}if (isS){y=Yadd;}if (isA){x=Xsub;}
function movefloat(){//Ysub,Yadd,Xsub,Xadd





}


//Autor: Manuel Janz Pereira necasjanz@gmail.com