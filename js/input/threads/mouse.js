
self.addEventListener('message', function(e) {
  var data = e.data;  
  var pageX = e.data.MouseRx.m1;
  var pageY = e.data.MouseRy.m2;
  var rotation = e.data.Rotations.r1;
  var rotationRx = e.data.Rotations.r2;
  var rotationZ = e.data.Rotations.r3;
    switch (data.cmd) {
    case 'mouse':
var workerResultM = ({
     MouseRxresult: -pageX - pageY/2,
     MouseRyresult: e.data.MouseRy.m2*2,
     MouseRxyresult: e.data.MouseRy.m2/10,
     MouseRXyresult: -e.data.MouseRx.m1,
     MouseFresultX: pageX,
     MouseFresultY: pageY     
 });
self.postMessage(workerResultM);
      break;
    case 'startMouse':
      self.postMessage('WORKER STARTED: ' + data.msg);
      self.start(); 
      break;
    case 'stopMouse':
      self.postMessage('WORKER STOPPED: ' + data.msg +
                       '. (Thread closed...! The mouse will no longer work)');
      //self.close(); // Terminates the worker.
      break;

    default:
      self.postMessage('Unknown command: ' + data.msg);
  };
}, false);
//Copyright (C) 2018 Manuel Janz Pereira
//Autor: Manuel Janz Pereira necasjanz@gmail.com