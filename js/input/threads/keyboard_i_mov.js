
self.addEventListener('message', function(e) {
  var data = e.data;  


    switch (data.cmd) {
    case 'loop':
var workerResult = ({
    YsubResult: e.data.YsubThis.op1 - e.data.YsubThis.op2,//Y sub
     YaddResult: e.data.YaddThis.op3 + e.data.YaddThis.op4,//Y add
    XsubResult: e.data.XsubThis.op5 - e.data.XsubThis.op6,//X sub
     XaddResult: e.data.XaddThis.op7 + e.data.XaddThis.op8,//X add
    XYcOResult: e.data.XYcOThis.op9 - e.data.XYcOThis.op10,//2d cursor offsetx
     YXcOResult: e.data.YXcOThis.op11 - e.data.YXcOThis.op12,//2d cursor offsety
    XYfOResult: -e.data.XYfOThis.op13 - e.data.XYfOThis.op14,//3d cursor offsetx
     YXfOResult: -e.data.YXfOThis.op15 - e.data.YXfOThis.op16,//3d cursor offsety
    XYfPResult: e.data.XYfPThis.op17 + e.data.XYfPThis.op18,//2d cursor offsetx center
     YXfPResult: e.data.YXfPThis.op19 + e.data.YXfPThis.op20,//2d cursor offsety center
     //fillResult: e.data.filleThis.f1 = e.data.filleThis.f2//maskfill


 });
self.postMessage(workerResult);
      break;
      
    case 'start':
      self.postMessage('WORKER STARTED: ' + data.msg);
      break;
    case 'stopMovement':
      self.postMessage('WORKER STOPPED: ' + data.msg +
                       '. (Thread closed...! The movement will no longer work)');
      self.close(); // Terminates the worker.
      break;

    default:
      self.postMessage('Unknown command: ' + data.msg);
  };
}, false);
//Copyright (C) 2018 Manuel Janz Pereira
//Autor: Manuel Janz Pereira necasjanz@gmail.com